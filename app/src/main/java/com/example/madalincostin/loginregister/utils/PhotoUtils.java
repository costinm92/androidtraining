package com.example.madalincostin.loginregister.utils;

import com.example.madalincostin.loginregister.persistence.bean.implementation.FavoritePhotoStore;
import com.example.madalincostin.loginregister.persistence.bean.implementation.PhotoBean;
import com.example.madalincostin.loginregister.persistence.bean.implementation.UserFavoritesBean;
import com.example.madalincostin.loginregister.persistence.bean.implementation.UserFavoritesStore;

import java.util.Date;

/**
 * Created by Madalin Costin
 * With Android Studio on 2015.
 */
public class PhotoUtils {
    public static boolean isPhotoFavorite(String photoId, String username) {
        return UserFavoritesStore.getInstance().isPhotoInUserList(photoId, username);
    }

    public static void addNewUserFavoriteBean(PhotoBean favoritePhotoBean) {
        FavoritePhotoStore.getInstance().add(favoritePhotoBean, true);
        UserFavoritesBean bean = new UserFavoritesBean(UserLocalStore.getInstance()
                .getLoggedInUser().getUsername(), favoritePhotoBean.getId(), new Date().getTime());
        UserFavoritesStore.getInstance().add(bean, true);
    }

    public static void removePhotoFromFavorites(UserFavoritesBean userFavoritesBean) {
        UserFavoritesStore.getInstance().remove(userFavoritesBean);
        FavoritePhotoStore.getInstance().removePhoto(userFavoritesBean.getPhotoId());
    }
}
