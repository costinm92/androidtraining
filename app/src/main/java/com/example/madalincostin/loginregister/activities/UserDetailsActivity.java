package com.example.madalincostin.loginregister.activities;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.madalincostin.loginregister.R;
import com.example.madalincostin.loginregister.fragments.UserDetailFragment;
import com.example.madalincostin.loginregister.utils.UserLocalStore;

public class UserDetailsActivity extends BaseActivity {
    /**
     * Creates an UserDetailFragment based on the username received on the intent.
     * @return the created UserDetailFragment.
     */
    private UserDetailFragment getUserDetailFragment() {
        UserDetailFragment userDetailFragment = null;
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey(USER_NAME)) {
            userDetailFragment = UserDetailFragment.newInstance(getIntent().getExtras().getString(USER_NAME));
        }
        return userDetailFragment;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);

        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, getUserDetailFragment())
                .commit();
        displayHomeButton();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case EDIT_REQUEST_CODE:
                    showToastMessage(getString(R.string.edit_user_success));
                    invalidateOptionsMenu();
                    break;
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.iEdit:
                Intent intent = new Intent(this, EditUserActivity.class);
                intent.putExtra(USER_NAME, UserLocalStore.getInstance().getLoggedInUser().getUsername());
                startActivityForResult(intent, EDIT_REQUEST_CODE);
                break;
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        if (isCurrentUserLoggedIn()) {
            getMenuInflater().inflate(R.menu.menu_loggedin_edit, menu);
        }
        return true;
    }
}
