package com.example.madalincostin.loginregister.utils;

import com.example.madalincostin.loginregister.persistence.bean.implementation.UserBean;

/**
 * Created by Madalin Costin
 * With Android Studio on 2015.
 */
public final class UserLocalStore {
    private static volatile UserLocalStore instance = null;
    private UserBean loggedInUser;
    private boolean loggedIn;

    protected UserLocalStore() {
        loggedIn = false;
    }

    public UserBean getLoggedInUser() {
        return loggedInUser;
    }

    public static UserLocalStore getInstance() {
        if (instance == null) {
            synchronized (UserLocalStore.class) {
                if (instance == null) {
                    instance = new UserLocalStore();
                }
            }
        }
        return instance;
    }

    public void setLoggedInUser(UserBean loggedInUser) {
        this.loggedInUser = loggedInUser;
        loggedIn = true;
    }

    public boolean getUserLoggedIn() {
        return loggedIn;
    }

    public void clearUserData() {
        loggedInUser = null;
        loggedIn = false;
    }
}
