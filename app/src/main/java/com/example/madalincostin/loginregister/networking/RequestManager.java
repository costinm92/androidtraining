package com.example.madalincostin.loginregister.networking;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.example.madalincostin.loginregister.enums.RequestType;
import com.example.madalincostin.loginregister.utils.RequestQueueManager;

/**
 * Created by Madalin Costin
 * With Android Studio on 2015.
 */
public class RequestManager {
    public static void submitGetPhotosRequest(Context context, String tag, RequestType requestType) {
        GetPhotosRequest photosRequest;
        if (tag == null) {
            photosRequest = new GetPhotosRequest("", requestType);
        } else {
            photosRequest = new GetPhotosRequest(tag.replace(" ", "%20"), requestType);
        }
        RequestQueue requestQueue = RequestQueueManager.getInstance(context.getApplicationContext()).getRequestQueue();
        requestQueue.add(photosRequest);
    }
}
