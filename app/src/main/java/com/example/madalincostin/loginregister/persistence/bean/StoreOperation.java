package com.example.madalincostin.loginregister.persistence.bean;

/**
 * This class models all the operation types that can be executed over a database
 *
 * @author Marian Lungu
 */
public final class StoreOperation {

    public static final StoreOperation ITEM_ADDED = new StoreOperation("ITEM_ADDED");
    public static final StoreOperation ITEMS_ADDED = new StoreOperation("ITEMS_ADDED");
    public static final StoreOperation ITEM_DELETED = new StoreOperation("ITEM_DELETED");
    public static final StoreOperation ITEMS_DELETED = new StoreOperation("ITEMS_DELETED");
    public static final StoreOperation ITEM_MODIFIED = new StoreOperation("ITEM_MODIFIED");

    private String mDescription;

    private StoreOperation(String desc) {
        mDescription = desc;
    }

    @Override
    public String toString() {
        return "StoreOperation{" + "description='" + mDescription + '\'' + '}';
    }

    /**
     * @return the description of this {@link StoreOperation}
     */
    public String getDescription() {
        return this.mDescription;
    }
}
