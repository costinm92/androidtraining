package com.example.madalincostin.loginregister.persistence;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.logging.Logger;

/**
 * The {@link SQLiteOpenHelper} type for this project. Used along with Singleton pattern.
 *
 * @author Marian Lungu
 */
public class ManagerDBOpenHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "manager_db";
    protected static final int DB_VERSION = 1;
    private static ManagerDBOpenHelper mInstance = null;
    protected static final Logger LOGGER = Logger.getLogger(ManagerDBOpenHelper.class.getSimpleName());

    /**
     * Public constructor. Calls super constructor.
     */
    public ManagerDBOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    /**
     * @param context The context of the {@link SQLiteOpenHelper}
     * @return the singleton instance of this class
     */
    public static ManagerDBOpenHelper getInstance(Context context) {
        if (mInstance == null) {
            synchronized (ManagerDBOpenHelper.class) {
                if (mInstance == null) {
                    mInstance = new ManagerDBOpenHelper(context, DB_NAME, null, DB_VERSION);
                }
            }
        }
        return mInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        /**
         *  TODO: create tables...must I?!
         */
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        LOGGER.warning("Upgrading DB from version " + oldVersion + "  to " + newVersion);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        LOGGER.warning("Downgrading DB from version " + oldVersion + "  to " + newVersion);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        LOGGER.warning("Database was opened!");
    }

    private static final class TableQueriesHelper {

        private String mTableName;
        private Column[] mColumns;

        private String mCreateQuery;

        TableQueriesHelper(String mTableName, Column[] mColumns) {
            this.mTableName = mTableName;
            this.mColumns = mColumns.clone();
            initQueries();
        }

        private Column[] getColumns() {
            return mColumns;
        }

        private String getTableName() {
            return mTableName;
        }

        private void initQueries() {
            String createColumns = "";
            String insertColumns = "";
            String insertValues = "";
            StringBuilder updateColumns = new StringBuilder();
            String selectColumns = "";

            String primaryKeyConstraint = generatePrimaryKeyConstraint(getColumns());

            for (int i = 0; i < getColumns().length; i++) {
                String separator = (i == getColumns().length - 1) ? "" : ", ";
                Column tableColumn = getColumns()[i];
                createColumns += tableColumn.getName() + " " + tableColumn.getType();

                if (primaryKeyConstraint == null) {
                    createColumns += " " + tableColumn.getConstraints();
                }
                createColumns += separator;
                selectColumns += tableColumn.getName() + separator;

                if (!tableColumn.hasConstraint(Column.CONSTRAINT_AUTOINCREMENT)) {
                    insertColumns += tableColumn.getName() + separator;
                    insertValues += "?" + separator;
                    updateColumns.append(tableColumn.getName() + " = ?" + separator);
                }
            }

            if (primaryKeyConstraint == null) {
                mCreateQuery = "CREATE TABLE IF NOT EXISTS " + getTableName() + "(" + createColumns + ")";
            } else {
                mCreateQuery = "CREATE TABLE IF NOT EXISTS " + getTableName() + "(" + createColumns + ", "
                        + primaryKeyConstraint + ")";
            }
        }

        private String generatePrimaryKeyConstraint(Column[] columns) {
            int primaryKeyCount = 0;
            StringBuffer multiplePrimaryKeyStr = new StringBuffer(Column.CONSTRAINT_PRIMARY_KEY);
            multiplePrimaryKeyStr.append("(");

            for (int i = 0; i < columns.length; i++) {
                Column tableColumn = columns[i];

                if (tableColumn.hasConstraint(Column.CONSTRAINT_PRIMARY_KEY)) {
                    primaryKeyCount++;
                    multiplePrimaryKeyStr.append(tableColumn.getName());
                    multiplePrimaryKeyStr.append(",");
                }
            }

            if (primaryKeyCount > 1) {
                return multiplePrimaryKeyStr.substring(0, multiplePrimaryKeyStr.length() - 1) + ")";
            }
            return null;
        }

        private String getCreateQuery() {
            return mCreateQuery;
        }
    }
}
