package com.example.madalincostin.loginregister.networking.events;

import com.example.madalincostin.loginregister.enums.Status;
import com.example.madalincostin.loginregister.enums.RequestType;

/**
 * Created by Madalin Costin
 * With Android Studio on 2015.
 */
public class ResponseEvent<T> {
    private T data;
    private Status status;
    private RequestType requestType;

    public ResponseEvent(T data, Status status, RequestType requestType) {
        this.data = data;
        this.status = status;
        this.requestType = requestType;
    }

    public T getData() {
        return data;
    }

    public Status getStatus() {
        return status;
    }

    public RequestType getRequestType() {
        return requestType;
    }
}
