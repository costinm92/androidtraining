package com.example.madalincostin.loginregister.networking.events;

import com.example.madalincostin.loginregister.enums.Status;
import com.example.madalincostin.loginregister.enums.RequestType;
import com.example.madalincostin.loginregister.persistence.bean.implementation.PhotoBean;

import java.util.List;

/**
 * Created by Madalin Costin
 * With Android Studio on 2015.
 */
public class PhotoSearchEvent extends ResponseEvent<List<PhotoBean>> {
    public PhotoSearchEvent(List<PhotoBean> data, Status status, RequestType requestType) {
        super(data, status, requestType);
    }
}
