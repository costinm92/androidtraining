package com.example.madalincostin.loginregister.fragments;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.madalincostin.loginregister.R;
import com.example.madalincostin.loginregister.activities.BaseActivity;
import com.example.madalincostin.loginregister.activities.ViewPhotoActivity;
import com.example.madalincostin.loginregister.adapters.ImageListAdapter;
import com.example.madalincostin.loginregister.networking.RequestManager;
import com.example.madalincostin.loginregister.enums.RequestType;
import com.example.madalincostin.loginregister.networking.events.ErrorEvent;
import com.example.madalincostin.loginregister.networking.events.PhotoSearchEvent;
import com.example.madalincostin.loginregister.enums.Status;
import com.example.madalincostin.loginregister.persistence.bean.implementation.PhotoBean;
import com.example.madalincostin.loginregister.persistence.bean.implementation.UserBean;
import com.example.madalincostin.loginregister.persistence.bean.implementation.UserFavoritesStore;
import com.example.madalincostin.loginregister.utils.PhotoUtils;
import com.example.madalincostin.loginregister.utils.UserLocalStore;

import de.greenrobot.event.EventBus;

public class PreferencesFragment extends Fragment {
    private ProgressDialog progress;
    private ListView preferencesView;
    private boolean isLoaded;
    private String preferenceLoaded;

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Favorites");

        if (v.getId() == R.id.preferencesView) {
            ListView lv = (ListView) v;
            AdapterView.AdapterContextMenuInfo adapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo) menuInfo;
            PhotoBean bean = (PhotoBean) lv.getItemAtPosition(adapterContextMenuInfo.position);
            if (PhotoUtils.isPhotoFavorite(bean.getId(), UserLocalStore.getInstance().getLoggedInUser().getUsername())) {
                menu.add(Menu.NONE, v.getId(), Menu.NONE, "Remove from favorites");
            } else {
                menu.add(Menu.NONE, v.getId(), Menu.NONE, "Add to favorites");
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo adapterContextMenuInfo =
                (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        PhotoBean photo = (PhotoBean) preferencesView.getItemAtPosition(adapterContextMenuInfo.position);
        UserBean user = UserLocalStore.getInstance().getLoggedInUser();

        if (item.getTitle() == "Add to favorites") {
            PhotoUtils.addNewUserFavoriteBean(photo);
            Toast.makeText(getActivity(), getString(R.string.photo_added_to_favorites), Toast.LENGTH_SHORT).show();
        } else if (item.getTitle() == "Remove from favorites") {
            PhotoUtils.removePhotoFromFavorites(UserFavoritesStore.getInstance().getPhoto(photo.getId(), user.getUsername()));
            Toast.makeText(getActivity(), getString(R.string.photo_removed_from_favorites), Toast.LENGTH_SHORT).show();
        } else {
            return false;
        }
        return true;
    }

    /**
     * Listener for an item click. It calls redirectToPhotoPage function with a photoBean parameter.
     */
    private AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Object obj = parent.getItemAtPosition(position);
            if (obj instanceof PhotoBean) {
                redirectToPhotoPage((PhotoBean) obj);
            }
        }
    };

    /**
     * Initializes the elements to be displayed. It needs to be called from the parent activity
     * each time the displayed data needs to be updated.
     */
    public void initializeElements() {
        if ((!isLoaded || preferenceLoaded != null && !preferenceLoaded.equals(UserLocalStore
                .getInstance().getLoggedInUser().getPreference())) && getActivity() != null) {
            progress = new ProgressDialog(getActivity());
            progress.setTitle("Loading");
            progress.setMessage("Wait while loading...");
            progress.setCancelable(false);
            progress.show();
            getPhotos();
        }
    }

    /**
     * Starts a new intent, sets as extra the PHOTO_IMAGE, PHOTO_ID and PHOTO_TITLE and starts
     * a new activity with this intent.
     * @param photo is the photo which gives the details for the new activity.
     */
    private void redirectToPhotoPage(PhotoBean photo) {
        Intent intent = new Intent(getActivity(), ViewPhotoActivity.class);
        intent.putExtra(BaseActivity.PHOTO_IMAGE, photo.getUrl());
        intent.putExtra(BaseActivity.PHOTO_ID, photo.getId());
        intent.putExtra(BaseActivity.PHOTO_TITLE, photo.getTitle());
        startActivity(intent);
    }

    /**
     * Calls the submitGetPhotosRequest from the RequestManager class in order to send a request
     * to get the photos to initialize the page.
     */
    protected void getPhotos() {
        RequestManager.submitGetPhotosRequest(getActivity(), UserLocalStore.getInstance()
                .getLoggedInUser().getPreference(), RequestType.PREFERENCES);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        EventBus.getDefault().register(this);
        return inflater.inflate(R.layout.fragment_preferences, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        preferencesView = (ListView) getActivity().findViewById(R.id.preferencesView);
        preferencesView.setOnItemClickListener(onItemClickListener);
        isLoaded = false;
        registerForContextMenu(preferencesView);
    }

    /**
     * EventBus onEvent method that gets called after a request was successfully made.
     * @param event is the event that is returned by the eventBus call.
     */
    public void onEventMainThread(PhotoSearchEvent event) {
        if (getView() != null && event.getRequestType().equals(RequestType.PREFERENCES)) {
            if (event.getStatus().equals(Status.SUCCESS)) {
                ImageListAdapter adapter = new ImageListAdapter(preferencesView.getContext(),
                        R.layout.custom_photo_item, event.getData());
                preferencesView.setAdapter(adapter);
                preferencesView.setEmptyView(getView().findViewById(R.id.noPhotos));
                progress.dismiss();
                if (event.getData() != null && !event.getData().isEmpty()) {
                    isLoaded = true;
                }
                preferenceLoaded = UserLocalStore.getInstance().getLoggedInUser().getPreference();
            } else {
                progress.dismiss();
                Toast.makeText(getView().getContext(), R.string.no_internet_connection, Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * EventBus onEvent method that gets called after a request was unsuccessfully made.
     * @param event is the event that is returned by the eventBus call.
     */
    public void onEventMainThread(ErrorEvent event) {
        if (getView() != null && event.getRequestType().equals(RequestType.PREFERENCES)) {
            progress.dismiss();
            Toast.makeText(getView().getContext(), R.string.no_internet_connection, Toast.LENGTH_LONG).show();
        }
    }
}