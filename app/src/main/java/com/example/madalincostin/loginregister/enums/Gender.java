package com.example.madalincostin.loginregister.enums;

/**
 * Created by Madalin Costin
 * With Android Studio on 2015.
 */
public enum Gender {
    UNKNOWN(0),
    MALE(1),
    FEMALE(2);
    int value;
    Gender(int value) {
        this.value = value;
    }
}
