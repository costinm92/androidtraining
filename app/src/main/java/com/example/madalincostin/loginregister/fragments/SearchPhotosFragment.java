package com.example.madalincostin.loginregister.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.madalincostin.loginregister.R;
import com.example.madalincostin.loginregister.activities.BaseActivity;
import com.example.madalincostin.loginregister.activities.ViewPhotoActivity;
import com.example.madalincostin.loginregister.adapters.ImageListAdapter;
import com.example.madalincostin.loginregister.networking.RequestManager;
import com.example.madalincostin.loginregister.enums.RequestType;
import com.example.madalincostin.loginregister.networking.events.ErrorEvent;
import com.example.madalincostin.loginregister.networking.events.PhotoSearchEvent;
import com.example.madalincostin.loginregister.enums.Status;
import com.example.madalincostin.loginregister.persistence.bean.implementation.PhotoBean;

import de.greenrobot.event.EventBus;

public class SearchPhotosFragment extends Fragment {
    private ListView searchView;
    private EditText searchText;
    private LinearLayout progressBarContainer;

    /**
     * Listener for Search button click. It displays the progress bar, calls the hideKeyboard
     * function and the getPhotos function.
     */
    private AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            redirectToPhotoPage((PhotoBean) parent.getItemAtPosition(position));
        }
    };

    /**
     * Listener for Search button click. It displays the progress bar, calls the hideKeyboard
     * function and the getPhotos function.
     */
    private View.OnClickListener onSearch = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            progressBarContainer.setVisibility(View.VISIBLE);
            getPhotos();
            hideKeyboard();
        }
    };

    /**
     * Hides the keyboard when it's called.
     */
    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(searchText.getWindowToken(), 0);
    }

    /**
     * Starts a new intent, sets as extra the PHOTO_IMAGE, PHOTO_ID and PHOTO_TITLE and starts
     * a new activity with this intent.
     * @param photo is the photo which gives the details for the new activity.
     */
    private void redirectToPhotoPage(PhotoBean photo) {
        Intent intent = new Intent(getActivity(), ViewPhotoActivity.class);
        intent.putExtra(BaseActivity.PHOTO_IMAGE, photo.getUrl());
        intent.putExtra(BaseActivity.PHOTO_ID, photo.getId());
        intent.putExtra(BaseActivity.PHOTO_TITLE, photo.getTitle());
        startActivity(intent);
    }

    /**
     * Calls the submitGetPhotosRequest from the RequestManager class in order to send a request
     * to get the photos to initialize the page.
     */
    protected void getPhotos() {
        RequestManager.submitGetPhotosRequest(getActivity(), searchText.getText().toString(), RequestType.SEARCH);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        EventBus.getDefault().register(this);
        return inflater.inflate(R.layout.fragment_search_photos, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        searchView = (ListView) getActivity().findViewById(R.id.searchView);
        searchView.setOnItemClickListener(onItemClickListener);
        final Button search = (Button) getActivity().findViewById(R.id.search);
        search.setOnClickListener(onSearch);
        searchText = (EditText) getActivity().findViewById(R.id.searchText);
        searchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    search.callOnClick();
                    return true;
                }
                return false;
            }
        });
        progressBarContainer = (LinearLayout) getActivity().findViewById(R.id.progressBarContainer);
    }

    /**
     * EventBus onEvent method that gets called after a request was successfully made.
     * @param event is the event that is returned by the eventBus call.
     */
    public void onEventMainThread(PhotoSearchEvent event) {
        if (getView() != null && event.getRequestType().equals(RequestType.SEARCH)) {
            if (event.getStatus().equals(Status.SUCCESS)) {
                searchView.setAdapter(new ImageListAdapter(searchView.getContext(),
                        R.layout.custom_photo_item, event.getData()));
                searchView.setEmptyView(getView().findViewById(R.id.noSearchPhotos));
                progressBarContainer.setVisibility(View.GONE);
            } else {
                progressBarContainer.setVisibility(View.GONE);
                Toast.makeText(getView().getContext(), R.string.no_internet_connection, Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * EventBus onEvent method that gets called after a request was unsuccessfully made.
     * @param event is the event that is returned by the eventBus call.
     */
    public void onEventMainThread(ErrorEvent event) {
        if (getView() != null && event.getRequestType().equals(RequestType.PREFERENCES)) {
            progressBarContainer.setVisibility(View.GONE);
            Toast.makeText(getView().getContext(), R.string.no_internet_connection, Toast.LENGTH_LONG).show();
        }
    }
}
