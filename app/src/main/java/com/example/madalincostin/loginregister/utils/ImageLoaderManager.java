package com.example.madalincostin.loginregister.utils;

import android.content.Context;
import android.graphics.Bitmap;

import com.android.volley.toolbox.ImageLoader;

/**
 * Created by Madalin Costin
 * With Android Studio on 2015.
 */
public class ImageLoaderManager {
    private static ImageLoaderManager instance;
    private static ImageLoader imageLoader;
    private static Context context;

    private ImageLoaderManager(Context context) {
        ImageLoaderManager.context = context;
    }

    public static ImageLoaderManager getInstance(Context context) {
        if (instance == null) {
            synchronized (ImageLoaderManager.class) {
                if (instance == null) {
                    instance = new ImageLoaderManager(context);
                }
            }
        }
        return instance;
    }

    public ImageLoader getImageLoader() {
        if (imageLoader == null) {
            imageLoader = new ImageLoader(RequestQueueManager.getInstance(context).getRequestQueue(), new ImageLoader.ImageCache() {
                @Override
                public Bitmap getBitmap(String url) {
                    return null;
                }

                @Override
                public void putBitmap(String url, Bitmap bitmap) {

                }
            });
        }
        return imageLoader;
    }
}
