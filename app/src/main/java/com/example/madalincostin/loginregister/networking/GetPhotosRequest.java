package com.example.madalincostin.loginregister.networking;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.example.madalincostin.loginregister.enums.RequestType;
import com.example.madalincostin.loginregister.networking.events.ErrorEvent;
import com.example.madalincostin.loginregister.networking.events.PhotoSearchEvent;
import com.example.madalincostin.loginregister.enums.Status;
import com.example.madalincostin.loginregister.persistence.bean.implementation.PhotoBean;
import com.example.madalincostin.loginregister.utils.FlickrUtils;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by Madalin Costin
 * With Android Studio on 2015.
 */
public class GetPhotosRequest extends Request<List<PhotoBean>> {
    private RequestType requestType;

    public GetPhotosRequest(String tag, RequestType requestType) {
        super(Request.Method.GET, "https://api.flickr.com/services/rest/?method=flickr.photos.search" +
                "&api_key=" + FlickrUtils.FLICKR_API_KEY + "&tags=" + tag + "&format=json&nojsoncallback=1" +
                "&per_page=10", null);
        setRetryPolicy(new DefaultRetryPolicy(1000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        this.requestType = requestType;
    }

    @Override
    protected Response<List<PhotoBean>> parseNetworkResponse(NetworkResponse response) {
        JsonObject jsonPhotos = new JsonParser().parse(new String(response.data)).getAsJsonObject()
                .getAsJsonObject("photos");
        List<PhotoBean> photos = new ArrayList<>();
        if (jsonPhotos != null && jsonPhotos.getAsJsonArray("photo") != null) {
            JsonArray jsonArray = jsonPhotos.getAsJsonArray("photo");
            for (JsonElement element : jsonArray) {
                String photoUrl = FlickrUtils.getImageUrl(
                        element.getAsJsonObject().get("farm").toString(),
                        element.getAsJsonObject().get("server").toString(),
                        element.getAsJsonObject().get("id").toString(),
                        element.getAsJsonObject().get("secret").toString());
                photos.add(new PhotoBean(element.getAsJsonObject().get("id").toString(),
                        element.getAsJsonObject().get("title").toString(), photoUrl));
            }
        }
        return Response.success(photos, HttpHeaderParser.parseCacheHeaders(response));
    }

    @Override
    protected VolleyError parseNetworkError(VolleyError volleyError) {
        EventBus.getDefault().post(new ErrorEvent(volleyError, Status.ERROR, requestType));
        return super.parseNetworkError(volleyError);
    }

    @Override
    protected void deliverResponse(List<PhotoBean> response) {
        EventBus.getDefault().post(new PhotoSearchEvent(response, Status.SUCCESS, requestType));
    }
}
