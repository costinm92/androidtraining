package com.example.madalincostin.loginregister.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.madalincostin.loginregister.R;
import com.example.madalincostin.loginregister.activities.BaseActivity;
import com.example.madalincostin.loginregister.persistence.bean.implementation.UserBean;
import com.example.madalincostin.loginregister.persistence.bean.implementation.UserStore;
import com.example.madalincostin.loginregister.enums.Gender;

public class UserDetailFragment extends Fragment {
    private TextView vName;
    private TextView vAge;
    private TextView vUsername;
    private TextView vDescription;
    private TextView vGender;
    private TextView vPreference;

    /**
     * Creates a new instance of this class and initializes it.
     * @param username is the unique key for getting the user to display its details.
     * @return an instance of UserDetailFragment.
     */
    public static UserDetailFragment newInstance(String username) {
        UserDetailFragment userDetailFragment = new UserDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putString(BaseActivity.USER_NAME, username);
        userDetailFragment.setArguments(bundle);
        return userDetailFragment;
    }

    /**
     * It sets the editViews text with the corresponding user details.
     * @param user is the user which provides the details to be set.
     */
    public void initializeElements(UserBean user) {
        if (user != null) {
            vName.setText(user.getName());
            if (user.getAge() != null && user.getAge() != -1) {
                vAge.setText(user.getAge() + "");
            } else {
                vAge.setText(BaseActivity.DEFAULT_VALUE);
            }
            vUsername.setText(user.getUsername());
            if (user.getDescription() == null || user.getDescription().isEmpty()) {
                vDescription.setText(BaseActivity.DEFAULT_VALUE);
            } else {
                vDescription.setText(user.getDescription());
            }
            if (user.getGender() == Gender.UNKNOWN) {
                vGender.setText(BaseActivity.DEFAULT_VALUE);
            } else {
                vGender.setText(user.getGender().toString());
            }
            if (user.getPreference() == null || user.getPreference().isEmpty()) {
                vPreference.setText(BaseActivity.DEFAULT_VALUE);
            } else {
                vPreference.setText(user.getPreference());
            }
        }
    }

    /**
     * Gets the username from a bundle and get the user with the username from database.
     * @return the user from the database.
     */
    private UserBean getUserFromArguments() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            return UserStore.getInstance().getUser(bundle.getString(BaseActivity.USER_NAME));
        }
        return null;
    }

    @Override
    public void onStart() {
        super.onStart();
        initializeElements(getUserFromArguments());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_user_detail, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        vName = (TextView) view.findViewById(R.id.vName);
        vAge = (TextView) view.findViewById(R.id.vAge);
        vUsername = (TextView) view.findViewById(R.id.vUsername);
        vDescription = (TextView) view.findViewById(R.id.vDescription);
        vGender = (TextView) view.findViewById(R.id.vGender);
        vPreference = (TextView) view.findViewById(R.id.vPreference);
    }
}
