package com.example.madalincostin.loginregister.persistence.bean.implementation;

import android.support.annotation.NonNull;

import com.example.madalincostin.loginregister.persistence.bean.Bean;

/**
 * Created by Madalin Costin
 * With Android Studio on 2015.
 */
public class PhotoBean implements Bean, Comparable<PhotoBean> {
    private String id;
    private String title;
    private String url;

    public PhotoBean() {
    }

    public PhotoBean(String id, String title, String url) {
        this.id = id;
        this.title = title;
        this.url = url;
    }

    @Override
    public Object[] getIdentity() {
        return new Object[]{id};
    }

    @Override
    public boolean requiresEncryption() {
        return false;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public int compareTo(@NonNull PhotoBean another) {
        if (another.getId() != null) {
            return another.getId().compareTo(this.getId());
        }
        return 1;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((title == null) ? 0 : title.hashCode());
        result = prime * result + ((url == null) ? 0 : url.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (((Object) this).getClass() != obj.getClass()) {
            return false;
        }
        PhotoBean other = (PhotoBean) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        if (title == null) {
            if (other.title != null) {
                return false;
            }
        } else if (!title.equals(other.title)) {
            return false;
        }
        if (url == null) {
            if (other.url != null) {
                return false;
            }
        } else if (!url.equals(other.url)) {
            return false;
        }
        return true;
    }
}
