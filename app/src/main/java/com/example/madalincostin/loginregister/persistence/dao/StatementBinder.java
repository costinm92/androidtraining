package com.example.madalincostin.loginregister.persistence.dao;

import android.database.sqlite.SQLiteStatement;

/**
 * {@link SQLiteStatement} throws an exception if you try to bind a null value. In this case {@link StatementBinder}
 * simply replaces the invocation with an invocation to {@link SQLiteStatement#bindNull(int)}.
 *
 * @author gabor.biro
 */
public class StatementBinder {

    private SQLiteStatement mStatement;

    /**
     * Public constructor
     *
     * @param statement the {@link SQLiteStatement} to bind
     */
    public StatementBinder(SQLiteStatement statement) {
        this.mStatement = statement;
    }

    /**
     * @param index the index of the value in the SQL statement
     */
    public void bindNull(int index) {
        mStatement.bindNull(index);
    }

    /**
     * @param index the index of the value in the SQL statement
     * @param value the long value to be binded
     */
    public void bindLong(int index, long value) {
        mStatement.bindLong(index, value);
    }

    /**
     * @param index the index of the value in the SQL statement
     * @param value the double value to be binded
     */
    public void bindDouble(int index, double value) {
        mStatement.bindDouble(index, value);
    }

    /**
     * @param index the index of the value in the SQL statement
     * @param value the string value to be binded
     */
    public void bindString(int index, String value) {
        if (value == null) {
            mStatement.bindNull(index);
        } else {
            mStatement.bindString(index, value);
        }
    }

    /**
     * @param index the index of the value in the SQL statement
     * @param value the blob to be binded
     */
    public void bindBlob(int index, byte[] value) {
        if (value == null) {
            mStatement.bindNull(index);
        } else {
            mStatement.bindBlob(index, value);
        }
    }
}
