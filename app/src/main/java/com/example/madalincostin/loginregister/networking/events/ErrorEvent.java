package com.example.madalincostin.loginregister.networking.events;

import com.android.volley.VolleyError;
import com.example.madalincostin.loginregister.enums.Status;
import com.example.madalincostin.loginregister.enums.RequestType;

/**
 * Created by Madalin Costin
 * With Android Studio on 2015.
 */
public class ErrorEvent extends ResponseEvent<VolleyError> {
    public ErrorEvent(VolleyError data, Status status, RequestType requestType) {
        super(data, status, requestType);
    }
}
