package com.example.madalincostin.loginregister.persistence.dao.implementation;

import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.madalincostin.loginregister.persistence.Column;
import com.example.madalincostin.loginregister.persistence.bean.implementation.UserFavoritesBean;
import com.example.madalincostin.loginregister.persistence.dao.AbstractSQLiteDAO;
import com.example.madalincostin.loginregister.persistence.dao.DAO;
import com.example.madalincostin.loginregister.persistence.dao.StatementBinder;

import java.util.Arrays;

import static com.example.madalincostin.loginregister.persistence.Column.CONSTRAINT_PRIMARY_KEY;
import static com.example.madalincostin.loginregister.persistence.Column.TYPE_LONG;
import static com.example.madalincostin.loginregister.persistence.Column.TYPE_TEXT;

/**
 * Created by Madalin Costin
 * With Android Studio on 2015.
 */
public class UserFavoritesDAO extends AbstractSQLiteDAO<UserFavoritesBean> implements DAO<UserFavoritesBean> {
    protected static final String TABLE_NAME = "user_favorites";

    protected static final Column[] COLUMNS = new Column[]{
            new Column("username", TYPE_TEXT, CONSTRAINT_PRIMARY_KEY),// 0
            new Column("photo_id", TYPE_TEXT, CONSTRAINT_PRIMARY_KEY), // 1
            new Column("timestamp", TYPE_LONG), // 2
    };

    public UserFavoritesDAO(SQLiteOpenHelper helper) {
        super(helper);
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public Column[] getColumns() {
        return Arrays.copyOf(COLUMNS, COLUMNS.length);
    }

    @Override
    protected UserFavoritesBean read(Cursor cursor) {
        UserFavoritesBean uerFavoritesBean = new UserFavoritesBean();
        uerFavoritesBean.setUsername(cursor.getString(0));
        uerFavoritesBean.setPhotoId(cursor.getString(1));
        uerFavoritesBean.setTimestamp(cursor.getLong(2));
        return uerFavoritesBean;
    }

    @Override
    protected void fill(UserFavoritesBean item, StatementBinder statement) {
        for (int i = 0; i < COLUMNS.length; i++) {
            bind(item, statement, i + 1, i);
        }
    }

    private void bind(UserFavoritesBean item, StatementBinder statement, int bindingPos, int columnPos) {
        switch (columnPos) {
            case 0:
                statement.bindString(bindingPos, item.getUsername());
                break;
            case 1:
                if (item.getPhotoId() != null) {
                    statement.bindString(bindingPos, item.getPhotoId());
                }
                break;
            case 2:
                if (item.getTimestamp() != null) {
                    statement.bindLong(bindingPos, item.getTimestamp());
                }
                break;
            default:
                break;
        }
    }
}
