package com.example.madalincostin.loginregister.networking;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import com.android.volley.toolbox.NetworkImageView;
import com.example.madalincostin.loginregister.R;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Madalin Costin
 * With Android Studio on 2015.
 */
@Deprecated
public class GetImageTask extends AsyncTask<Void, Void, Bitmap> {
    private NetworkImageView networkImageView;
    private String url;

    public GetImageTask(NetworkImageView networkImageView, String url) {
        this.networkImageView = networkImageView;
        this.url = url;
    }

    @Override
    protected Bitmap doInBackground(Void... params) {
        try {
            InputStream in = new java.net.URL(url).openStream();
            return BitmapFactory.decodeStream(in);
        } catch (IOException ignored) {
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        networkImageView.setTag(url);
        networkImageView.setImageResource(R.drawable.ic_default_photo);
    }

    protected void onPostExecute(Bitmap result) {
        if (result != null && networkImageView.getTag() instanceof String
                && (networkImageView.getTag()).equals(url)) {
            networkImageView.setImageBitmap(result);
        }
    }
}
