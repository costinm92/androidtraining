package com.example.madalincostin.loginregister.persistence.bean.implementation;

import com.example.madalincostin.loginregister.persistence.ManagerDBOpenHelper;
import com.example.madalincostin.loginregister.persistence.bean.BeanFilter;
import com.example.madalincostin.loginregister.persistence.bean.BeanStoreImpl;
import com.example.madalincostin.loginregister.persistence.dao.DAO;
import com.example.madalincostin.loginregister.persistence.dao.implementation.FavoritePhotosDAO;
import com.example.madalincostin.loginregister.utils.ManagerApplication;

import java.util.List;

/**
 * Created by Madalin Costin
 * With Android Studio on 2015.
 */
public class FavoritePhotoStore extends BeanStoreImpl<PhotoBean> {
    private static FavoritePhotoStore sInstance;

    public static FavoritePhotoStore getInstance() {
        if (sInstance == null) {
            synchronized (FavoritePhotoStore.class) {
                if (sInstance == null) {
                    sInstance = new FavoritePhotoStore();
                }
            }
        }
        return sInstance;
    }

    @Override
    public DAO<PhotoBean> getDAO() {
        return new FavoritePhotosDAO(ManagerDBOpenHelper.getInstance(ManagerApplication.getInstance()));
    }

    public List<PhotoBean> getAllPhotos(final List<String> photoIds) {
        return super.getItems(new BeanFilter<PhotoBean>() {
            @Override
            public boolean accept(PhotoBean bean) {
                return photoIds.contains(bean.getId());
            }
        }, null);
    }

    public void removePhoto(final String photoId) {
        List<PhotoBean> beans = super.getItems(new BeanFilter<PhotoBean>() {
            @Override
            public boolean accept(PhotoBean bean) {
                return bean.getId().equals(photoId);
            }
        }, null);
        if (beans != null && !beans.isEmpty()) {
            remove(beans.get(0));
        }
    }
}
