package com.example.madalincostin.loginregister.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.example.madalincostin.loginregister.R;
import com.example.madalincostin.loginregister.activities.BaseActivity;
import com.example.madalincostin.loginregister.activities.ViewPhotoActivity;
import com.example.madalincostin.loginregister.adapters.ImageListAdapter;
import com.example.madalincostin.loginregister.persistence.bean.implementation.FavoritePhotoStore;
import com.example.madalincostin.loginregister.persistence.bean.implementation.PhotoBean;
import com.example.madalincostin.loginregister.persistence.bean.implementation.UserFavoritesBean;
import com.example.madalincostin.loginregister.persistence.bean.implementation.UserFavoritesStore;
import com.example.madalincostin.loginregister.utils.UserLocalStore;

import java.util.ArrayList;
import java.util.List;

public class FavoritesFragment extends Fragment {
    private ListView favoritesView;
    private LinearLayout progressBarContainer;

    /**
     * Listener for an item click. It calls redirectToPhotoPage function with a photoBean parameter.
     */
    private AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            redirectToPhotoPage((PhotoBean) parent.getItemAtPosition(position));
        }
    };

    /**
     * Starts a new intent, sets as extra the PHOTO_IMAGE, PHOTO_ID and PHOTO_TITLE and starts
     * a new activity with this intent.
     * @param photo is the photo which gives the details for the new activity.
     */
    private void redirectToPhotoPage(PhotoBean photo) {
        Intent intent = new Intent(getActivity(), ViewPhotoActivity.class);
        intent.putExtra(BaseActivity.PHOTO_IMAGE, photo.getUrl());
        intent.putExtra(BaseActivity.PHOTO_ID, photo.getId());
        intent.putExtra(BaseActivity.PHOTO_TITLE, photo.getTitle());
        startActivity(intent);
    }

    /**
     * Initializes the elements to be displayed. It needs to be called from the parent activity
     * each time the displayed data needs to be updated.
     */
    public void initializeElements() {
        List<String> photoIds = new ArrayList<>();
        for (UserFavoritesBean bean : UserFavoritesStore.getInstance().getAllUsersFavorites(
                UserLocalStore.getInstance().getLoggedInUser().getUsername())) {
            photoIds.add(bean.getPhotoId());
        }
        List<PhotoBean> photos = FavoritePhotoStore.getInstance().getAllPhotos(photoIds);

        favoritesView.setAdapter(new ImageListAdapter(favoritesView.getContext(),
                R.layout.custom_photo_item, photos));
        favoritesView.setEmptyView(getActivity().findViewById(R.id.noFavoritesPhotos));
        progressBarContainer.setVisibility(View.GONE);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_favorites, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        favoritesView = (ListView) getActivity().findViewById(R.id.favoritesView);
        favoritesView.setOnItemClickListener(onItemClickListener);
        progressBarContainer = (LinearLayout) getActivity().findViewById(R.id.progressBarFavoritesContainer);
    }

    @Override
    public void onResume() {
        super.onResume();
        initializeElements();
    }
}
