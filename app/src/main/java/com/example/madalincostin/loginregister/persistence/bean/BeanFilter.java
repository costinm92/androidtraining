package com.example.madalincostin.loginregister.persistence.bean;

/**
 * Generic class for all types which will filter a subset of beans from a larger list
 *
 * @param <E> the Bean type for which this filter will operate
 * @author Marian Lungu
 */
public interface BeanFilter<E extends Bean> {

    /**
     * @param bean The {@link Bean} to be inspected
     * @return true, if the filter should validate this {@link Bean} and accept it, false otherwise
     */
    boolean accept(E bean);
}
