package com.example.madalincostin.loginregister.persistence.dao.implementation;

import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.madalincostin.loginregister.persistence.Column;
import com.example.madalincostin.loginregister.persistence.bean.implementation.UserBean;
import com.example.madalincostin.loginregister.persistence.dao.AbstractSQLiteDAO;
import com.example.madalincostin.loginregister.persistence.dao.DAO;
import com.example.madalincostin.loginregister.persistence.dao.StatementBinder;
import com.example.madalincostin.loginregister.enums.Gender;

import java.util.Arrays;

import static com.example.madalincostin.loginregister.persistence.Column.CONSTRAINT_PRIMARY_KEY;
import static com.example.madalincostin.loginregister.persistence.Column.TYPE_INTEGER;
import static com.example.madalincostin.loginregister.persistence.Column.TYPE_TEXT;

/**
 * Created by Madalin Costin
 * With Android Studio on 2015.
 */
public class UsersDAO extends AbstractSQLiteDAO<UserBean> implements DAO<UserBean> {
    public static final String TABLE_NAME = "users";

    protected static final Column[] COLUMNS = new Column[]{
            new Column("name", TYPE_TEXT), // 0
            new Column("username", TYPE_TEXT, CONSTRAINT_PRIMARY_KEY), // 1
            new Column("description", TYPE_TEXT),// 2
            new Column("password", TYPE_TEXT), // 3
            new Column("age", TYPE_INTEGER), // 4
            new Column("gender", TYPE_TEXT), // 5
            new Column("preference", TYPE_TEXT), // 6
    };

    public UsersDAO(SQLiteOpenHelper helper) {
        super(helper);
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public Column[] getColumns() {
        return Arrays.copyOf(COLUMNS, COLUMNS.length);
    }

    @Override
    protected UserBean read(Cursor cursor) {
        UserBean user = new UserBean();
        user.setName(cursor.getString(0));
        user.setUsername(cursor.getString(1));
        user.setDescription(cursor.getString(2));
        user.setPassword(cursor.getString(3));
        user.setAge(cursor.getInt(4));
        user.setGender(Gender.valueOf(cursor.getString(5)));
        user.setPreference(cursor.getString(6));
        return user;
    }

    @Override
    protected void fill(UserBean item, StatementBinder statement) {
        for (int i = 0; i < COLUMNS.length; i++) {
            bind(item, statement, i + 1, i);
        }
    }

    private void bind(UserBean item, StatementBinder statement, int bindingPos, int columnPos) {
        switch (columnPos) {
            case 0:
                if (item.getName() != null) {
                    statement.bindString(bindingPos, item.getName());
                }
                break;
            case 1:
                if (item.getUsername() != null) {
                    statement.bindString(bindingPos, item.getUsername());
                }
                break;
            case 2:
                if (item.getDescription() != null) {
                    statement.bindString(bindingPos, item.getDescription());
                }
                break;
            case 3:
                if (item.getPassword() != null) {
                    statement.bindString(bindingPos, item.getPassword());
                }
                break;
            case 4:
                if (item.getAge() != null) {
                    statement.bindLong(bindingPos, item.getAge());
                }
                break;
            case 5:
                if (item.getGender() != null) {
                    statement.bindString(bindingPos, item.getGender().name());
                }
                break;
            case 6:
                if (item.getPreference() != null) {
                    statement.bindString(bindingPos, item.getPreference());
                }
                break;
            default:
                break;
        }
    }
}
