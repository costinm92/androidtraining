package com.example.madalincostin.loginregister.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.madalincostin.loginregister.R;
import com.example.madalincostin.loginregister.persistence.bean.implementation.UserBean;
import com.example.madalincostin.loginregister.persistence.bean.implementation.UserStore;

public class RegisterActivity extends BaseActivity {
    private EditText etName;
    private EditText etUsername;
    private EditText etPassword;
    private Button bRegister;

    /**
     * Checks if an user is valid.
     * @param user is the user to be checked.
     * @return a boolean value: true if the user is valid and false if not.
     */
    private boolean isUserValid(UserBean user) {
        return !(user.getName().length() < 3 || user.getUsername().length() < 3
                || user.getPassword().length() < 3);
    }

    /**
     * Listener for Register button click. It checks if the user is valid.
     * If it is valid, it sets the result as RESULT_OK and destroys the activity.
     * If it is not valid, it shows an informative toast message.
     */
    private View.OnClickListener onRegisterClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            UserStore userStore = UserStore.getInstance();
            UserBean user = new UserBean(etName.getText().toString(), etUsername.getText().toString(),
                    etPassword.getText().toString());

            if (isUserValid(user)) {
                UserBean usersBean = userStore.getUser(user.getUsername());
                if (usersBean == null) {
                    userStore.add(user, false);
                    Intent intent = new Intent();
                    intent.putExtra("name", user.getName());
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                } else {
                    showToastMessage("User with name " + user.getName() + " already exists!");
                }
            } else {
                showToastMessage(getString(R.string.invalid_user));
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        etName = (EditText) findViewById(R.id.etName);
        etUsername = (EditText) findViewById(R.id.etUsername);
        etPassword = (EditText) findViewById(R.id.etPassword);
        bRegister = (Button) findViewById(R.id.bRegister);
        bRegister.setOnClickListener(onRegisterClick);

        TextView.OnEditorActionListener editorActionListener = new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    bRegister.callOnClick();
                    return true;
                }
                return false;
            }
        };

        etName.setOnEditorActionListener(editorActionListener);
        etUsername.setOnEditorActionListener(editorActionListener);
        etPassword.setOnEditorActionListener(editorActionListener);
    }
}
