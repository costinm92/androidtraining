package com.example.madalincostin.loginregister.utils;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by Madalin Costin
 * With Android Studio on 2015.
 */
public class RequestQueueManager {
    private static RequestQueueManager instance;
    private RequestQueue requestQueue;
    private static Context context;

    private RequestQueueManager(Context context) {
        RequestQueueManager.context = context;
    }

    public static RequestQueueManager getInstance(Context context) {
        if (instance == null) {
            synchronized (RequestQueueManager.class) {
                if (instance == null) {
                    instance = new RequestQueueManager(context);
                }
            }
        }
        return instance;
    }

    public RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(context);
            requestQueue.start();
        }
        return requestQueue;
    }
}
