package com.example.madalincostin.loginregister.persistence.bean.implementation;

import com.example.madalincostin.loginregister.persistence.ManagerDBOpenHelper;
import com.example.madalincostin.loginregister.persistence.bean.BeanFilter;
import com.example.madalincostin.loginregister.persistence.bean.BeanStoreImpl;
import com.example.madalincostin.loginregister.persistence.dao.DAO;
import com.example.madalincostin.loginregister.persistence.dao.implementation.UsersDAO;
import com.example.madalincostin.loginregister.utils.ManagerApplication;

import java.util.List;

/**
 * Created by Madalin Costin
 * With Android Studio on 2015.
 */
public class UserStore extends BeanStoreImpl<UserBean> {
    private static UserStore sInstance;

    public static UserStore getInstance() {
        if (sInstance == null) {
            synchronized (UserStore.class) {
                if (sInstance == null) {
                    sInstance = new UserStore();
                }
            }
        }
        return sInstance;
    }

    @Override
    public DAO<UserBean> getDAO() {
        return new UsersDAO(ManagerDBOpenHelper.getInstance(ManagerApplication.getInstance()));
    }

    public List<UserBean> getAllUsers() {
        return super.getItems(new BeanFilter<UserBean>() {
            @Override
            public boolean accept(UserBean bean) {
                return true;
            }
        }, null);
    }

    public UserBean getUser(final String username) {
        List<UserBean> users = super.getItems(new BeanFilter<UserBean>() {
            @Override
            public boolean accept(UserBean bean) {
                return bean.getUsername().equals(username);
            }
        }, null);
        if (users != null && !users.isEmpty()) {
            return users.get(0);
        } else {
            return null;
        }
    }
}
