package com.example.madalincostin.loginregister.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.example.madalincostin.loginregister.R;
import com.example.madalincostin.loginregister.persistence.bean.implementation.PhotoBean;
import com.example.madalincostin.loginregister.utils.ImageLoaderManager;

import java.util.List;

/**
 * Created by Madalin Costin
 * With Android Studio on 2015.
 */
public class ImageListAdapter extends ArrayAdapter<PhotoBean> {
    private LayoutInflater inflater;

    public ImageListAdapter(Context context, int resource, List<PhotoBean> items) {
        super(context, resource, items);
        inflater = LayoutInflater.from(getContext());
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.custom_photo_item, null);
            holder = new ViewHolder();
            holder.title = (TextView) convertView.findViewById(R.id.firstLine);
            holder.image = (NetworkImageView) convertView.findViewById(R.id.icon);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        PhotoBean photo = getItem(position);
        if (photo != null) {
            if (holder.title != null) {
                holder.title.setText(photo.getTitle());
            }
            if (photo.getUrl() != null) {
                holder.image.setImageUrl(photo.getUrl(), ImageLoaderManager.getInstance(getContext()).getImageLoader());
            }
        }
        return convertView;
    }

    private class ViewHolder {
        TextView title;
        NetworkImageView image;
    }
}