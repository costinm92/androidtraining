package com.example.madalincostin.loginregister.persistence.dao;

import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.text.TextUtils;

import com.example.madalincostin.loginregister.persistence.Column;
import com.example.madalincostin.loginregister.persistence.bean.Bean;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Abstract implementation of {@link DAO} which specifies the instructions for CRUD operations: creating, listing,
 * updating, deleting entries from the DB
 *
 * @param <E> the sub-class of {@link Bean} that this SQLiteDAO will be handling
 * @author Marian Lungu
 */
public abstract class AbstractSQLiteDAO<E extends Bean> implements DAO<E> {
    private static final Logger sLogger = Logger.getLogger(AbstractSQLiteDAO.class.getSimpleName());

    protected String mCreateQuery;
    protected String mInsertQuery;
    protected String mInsertOrReplaceQuery;
    protected String mDeleteQuery;
    protected String mSelectQuery;
    protected String mUpdateQuery;
    protected String mPartialUpdateQuery;
    protected SQLiteOpenHelper mDbhelper;

    protected AbstractSQLiteDAO(SQLiteOpenHelper helper) {
        this.mDbhelper = helper;
        SQLiteDatabase db;
        initQueries();
        db = mDbhelper.getWritableDatabase();
        db.execSQL(getCreateQuery());
    }

    /**
     * @return the name of the table that will store the data into the db
     */
    public abstract String getTableName();

    /**
     * @return the columns of the table representing the data in the db
     */
    public abstract Column[] getColumns();

    /**
     * @param identity the array of objects representing the primary key set
     * @return a string representing the filtering sequence for a given column configuration + primary key value
     */
    public final String getIdentityFilter(Object... identity) {
        StringBuffer idFilter = new StringBuffer();
        int index = 0;
        Column[] columns = getColumns();

        for (Column column : columns) {
            if (column.hasConstraint(Column.CONSTRAINT_PRIMARY_KEY)) {
                idFilter.append(column.getFilter(identity[index++]));
                idFilter.append(" AND ");
            }
        }

        if (idFilter.length() > 0) {
            return idFilter.substring(0, idFilter.length() - 5);
        }
        return null;
    }

    /**
     * @param items the items to be matched by the filter
     * @return a filter sequence which will match all the items passed as a parameter
     */
    public final String getArrayFilter(E[] items) {
        Column[] columns = getColumns();
        List<Column> keyColumns = new ArrayList<Column>();

        for (Column column : columns) {
            if (column.hasConstraint(Column.CONSTRAINT_PRIMARY_KEY)) {
                keyColumns.add(column);
            }
        }

        /**
         * TODO FIXME the following test creates a major issue when trying to perform updates on a
         * table with a composed primary key (has multiple primary key columns) !!
         * In order to perform multiple deletes on such table, it would be better to perform a simple
         * delete statement for each item to be deleted
         */
        if (keyColumns.size() == 1) {
            StringBuffer filter = new StringBuffer(keyColumns.get(0).getName() + " IN (");

            for (E item : items) {
                filter.append(item.getIdentity()[0] + ",");
            }
            return (items.length > 0 ? filter.substring(0, filter.length() - 1) : filter) + ")";
        }
        return null;
    }

    /**
     * Creates the insert, create, update, select, delete queries
     */
    protected void initQueries() {
        String createColumns = "";
        String insertColumns = "";
        String insertValues = "";
        StringBuilder updateColumns = new StringBuilder();
        String selectColumns = "";

        Column[] columns = getColumns();
        String primaryKeyConstraint = generatePrimaryKeyConstraint(columns);

        for (int i = 0; i < columns.length; i++) {
            String separator = (i == columns.length - 1) ? "" : ", ";
            Column tableColumn = columns[i];

            createColumns += tableColumn.getName() + " " + tableColumn.getType();

            if (primaryKeyConstraint == null) {
                createColumns += " " + tableColumn.getConstraints();
            }
            createColumns += separator;
            selectColumns += tableColumn.getName() + separator;

            if (!tableColumn.hasConstraint(Column.CONSTRAINT_AUTOINCREMENT)) {
                insertColumns += tableColumn.getName() + separator;
                insertValues += "?" + separator;
                updateColumns.append(tableColumn.getName() + " = ?" + separator);
            }
        }

        if (primaryKeyConstraint == null) {
            mCreateQuery = "CREATE TABLE IF NOT EXISTS " + getTableName() + "(" + createColumns + ")";
        } else {
            mCreateQuery = "CREATE TABLE IF NOT EXISTS " + getTableName() + "(" + createColumns + ", "
                    + primaryKeyConstraint + ")";
        }
        mInsertQuery = "INSERT INTO " + getTableName() + "(" + insertColumns + ")" + " VALUES(" + insertValues + ")";
        mInsertOrReplaceQuery = "INSERT OR REPLACE INTO " + getTableName() + "(" + insertColumns + ")" + " VALUES("
                + insertValues + ")";
        mUpdateQuery = "UPDATE " + getTableName() + " SET " + updateColumns.toString();
        mSelectQuery = "SELECT " + selectColumns + " FROM " + getTableName();
        mDeleteQuery = "DELETE FROM " + getTableName();
    }

    /**
     * create an update query with a limited set of columns
     *
     * @param excludedColumns columns list that are excluded from update query
     */
    public void createPartialUpdateQuery(List<String> excludedColumns) {
        StringBuilder updateColumns = new StringBuilder();
        String separator = "";
        boolean needSeparator = false;

        Column[] columns = getColumns();
        for (int i = 0; i < columns.length; i++) {
            separator = (!needSeparator) ? "" : " , ";
            Column tableColumn = columns[i];

            if (!tableColumn.hasConstraint(Column.CONSTRAINT_AUTOINCREMENT) && excludedColumns != null
                    && !excludedColumns.contains(tableColumn.getName())) {
                needSeparator = true;
                updateColumns.append(separator).append(tableColumn.getName()).append(" = ?");
            }
        }
        mPartialUpdateQuery = "UPDATE " + getTableName() + " SET " + updateColumns.toString();
    }

    /**
     * Generate an SQLite constraint for primary keys but only if there is a
     * multi-column key.
     *
     * @return a string of the form 'PRIMARY KEY (key1, key2,...)' or null if
     * there's only one primary key
     */
    private static String generatePrimaryKeyConstraint(Column[] columns) {
        int primaryKeyCount = 0;
        StringBuffer multiplePrimaryKeyStr = new StringBuffer(Column.CONSTRAINT_PRIMARY_KEY);
        multiplePrimaryKeyStr.append("(");

        for (int i = 0; i < columns.length; i++) {
            Column tableColumn = columns[i];

            if (tableColumn.hasConstraint(Column.CONSTRAINT_PRIMARY_KEY)) {
                primaryKeyCount++;
                multiplePrimaryKeyStr.append(tableColumn.getName());
                multiplePrimaryKeyStr.append(",");
            }
        }

        if (primaryKeyCount > 1) {
            return multiplePrimaryKeyStr.substring(0, multiplePrimaryKeyStr.length() - 1) + ")";
        }
        return null;
    }

    protected String getCreateQuery() {
        return mCreateQuery;
    }

    protected String getInsertQuery(boolean replaceOnConflict) {
        if (replaceOnConflict) {
            return mInsertOrReplaceQuery;
        }
        return mInsertQuery;
    }

    protected String getDeleteQuery() {
        return mDeleteQuery;
    }

    protected String getSelectQuery() {
        return mSelectQuery;
    }

    protected String getUpdateQuery(E item) {
        return mUpdateQuery;
    }

    protected String getPartialUpdateQuery() {
        return mPartialUpdateQuery;
    }

    protected void setPartialUpdateQuery(String partialUpdateQuery) {
        this.mPartialUpdateQuery = partialUpdateQuery;
    }

    protected SQLiteOpenHelper getSQLiteOpenHelper() {
        return mDbhelper;
    }

    protected void beforeInsert(E item, SQLiteDatabase db) {
        /**
         *  override if necessary
         */
    }

    protected void afterInsert(E item, SQLiteDatabase db) {
        /**
         *  override if necessary
         */
    }

    /**
     * Makes all encryption parameters before a bean will be stored in the database
     *
     * @param item the item which will be encrypted
     * @return the encrypted bean, if the bean supports encryption, the same bean otherwise
     */
    @SuppressWarnings("unchecked")
    protected E prepareEncryption(E item) {
        E output = item;
        return output;
    }

    /**
     * Makes all decryption parameters after a bean was retrieved in the database
     *
     * @param item the item which will be decrypted
     * @return the decrypted bean, if the bean supports encryption, the same bean otherwise
     */
    @SuppressWarnings("unchecked")
    protected E prepareDecryption(E item) {
        E output = item;
        return output;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void insert(E item, boolean updateIfExists) {
        SQLiteDatabase db = getSQLiteOpenHelper().getWritableDatabase();
        insert(item, db, updateIfExists);
    }

    /**
     * @param item           the item to be inserted
     * @param db             the {@link SQLiteDatabase} instance
     * @param updateIfExists
     */
    public void insert(E item, SQLiteDatabase db, boolean updateIfExists) {
        SQLiteStatement statement = db.compileStatement(getInsertQuery(updateIfExists));
        beforeInsert(item, db);
        E itemToBeSaved = prepareEncryption(item);
        try {
            fill(itemToBeSaved, new StatementBinder(statement));
            statement.execute();
        } catch (SQLiteConstraintException e) {
            sLogger.log(Level.SEVERE, e.getMessage(), e);
        } finally {
            statement.close();
        }
        afterInsert(itemToBeSaved, db);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void insert(List<E> items, boolean updateIfExists) {
        SQLiteDatabase db = getSQLiteOpenHelper().getWritableDatabase();
        db.beginTransaction();
        try {
            insert(items, db, updateIfExists);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    /**
     * @param items          a list of items to be inserted
     * @param db             the {@link SQLiteDatabase} instance
     * @param updateIfExists
     */
    public void insert(List<E> items, SQLiteDatabase db, boolean updateIfExists) {
        for (E item : items) {
            insert(item, db, updateIfExists);
        }
    }

    /**
     * @param rows   the rows to be updated
     * @param filter a filter sequence
     */
    public void updateRows(List<String> rows, String filter) {
        SQLiteDatabase db = getSQLiteOpenHelper().getWritableDatabase();
        updateRows(rows, filter, db);
    }

    private void updateRows(List<String> rows, String filter, SQLiteDatabase db) {
        StringBuilder query = new StringBuilder("UPDATE " + getTableName() + " SET ");
        boolean isFirst = true;

        for (String row : rows) {
            if (!isFirst) {
                query.append(", ");
            } else {
                isFirst = false;
            }
            query.append(row);
        }
        query = new StringBuilder(applyFilter(query.toString(), filter));
        db.execSQL(query.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteAll() {
        String query = getDeleteQuery();
        SQLiteDatabase db = getSQLiteOpenHelper().getWritableDatabase();
        db.execSQL(query);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update(E item) {
        SQLiteDatabase db = getSQLiteOpenHelper().getWritableDatabase();
        update(item, db);
    }

    /**
     * @param item the item to be updated
     * @param db   the {@link SQLiteDatabase} instance
     */
    public void update(E item, SQLiteDatabase db) {
        String query = getUpdateQuery(item);
        String filter = getIdentityFilter(item.getIdentity());
        String finalQuery = applyFilter(query, filter);

        SQLiteStatement statement = db.compileStatement(finalQuery);
        try {
            fill(item, new StatementBinder(statement));
            statement.execute();
        } catch (SQLiteConstraintException e) {
        } finally {
            statement.close();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(E item) {
        E itemToBeDeleted = prepareEncryption(item);

        String filter = getIdentityFilter(itemToBeDeleted.getIdentity());

        if (!TextUtils.isEmpty(filter)) {
            deleteByFilter(filter);
        }
    }

    /**
     * TODO this method is not safe to be used in the case of composed primary key tables!!
     * <p/>
     * {@inheritDoc}
     */
    @Override
    public void deleteAll(E[] items) {
        for (int i = 0; i < items.length; i++) {
            E preparedItem = prepareEncryption(items[i]);
            items[i] = preparedItem;
        }

        String filter = getArrayFilter(items);

        if (!TextUtils.isEmpty(filter)) {
            deleteByFilter(filter);
        }
    }

    /**
     * Deletes entries from the database that match a certain filter
     *
     * @param filter the filter to mark-up the entries to delete
     */
    public void deleteByFilter(String filter) {
        SQLiteDatabase db = getSQLiteOpenHelper().getWritableDatabase();
        delete(filter, db);
    }

    /**
     * Deletes entries from the database that match a certain filter
     *
     * @param filter the filter to mark-up the entries to delete
     * @param db     the {@link SQLiteDatabase} instance
     */
    public void delete(String filter, SQLiteDatabase db) {
        String query = getDeleteQuery();
        query = applyFilter(query, filter);
        db.execSQL(query);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E get(Object... identity) {
        List<E> result = list(getIdentityFilter(identity), null, null);

        if (result.size() > 0) {
            return result.get(0);
        }
        return null;
    }

    /**
     * @return a list of elements;
     */
    public List<E> list() {
        return list(null, null, null);
    }

    /**
     * @return a list of elements;
     */
    public List<E> list(int pageSize, int page) {
        return list(null, (pageSize * page) + "," + pageSize, null);
    }

    /**
     * @param filter the filter to select certain entries
     * @param limit  the max-limit of results
     * @param order  the order to be applied to the selected entries
     * @return a list of elements which match the constraints passed as parameters
     */
    public List<E> list(String filter, String limit, String order) {
        List<E> items = new ArrayList<E>();

        String query = getSelectQuery();
        query = applyFilter(query, filter);
        query = applyOrder(query, order);
        query = applyLimit(query, limit);

        SQLiteDatabase db = getSQLiteOpenHelper().getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        try {
            while (cursor.moveToNext()) {
                E item = read(cursor);
                item = prepareDecryption(item);
                items.add(item);
            }
        } finally {
            cursor.close();
            cursor = null;
        }

        return items;
    }

    /**
     * Applies a custom select filter to the database
     *
     * @param fields the fields to be selected
     * @param filter the filter to be applied to the columns
     * @param limit  the max number of results
     * @param order  the order in which the results will be returned
     * @return a list of elements which match the constraints passed as parameters
     */
    public List<Object> customSelect(List<String> fields, String filter, String limit, String order) {
        List<Object> result;
        SQLiteDatabase db = getSQLiteOpenHelper().getReadableDatabase();
        result = customSelect(db, fields, filter, limit, order);

        return result;
    }

    private List<Object> customSelect(SQLiteDatabase db, List<String> fields, String filter, String limit, String order) {
        List<Object> items = new ArrayList<Object>();

        StringBuilder query = new StringBuilder("SELECT ");
        String finalQuery;
        boolean isFirst = true;

        for (Object o : fields) {
            if (o == null) {
                continue;
            }

            if (!isFirst) {
                query.append(", ");
            } else {
                isFirst = false;
            }
            query.append(" " + o);
        }
        query.append(" FROM " + getTableName());
        finalQuery = applyFilter(query.toString(), filter);
        finalQuery = applyOrder(finalQuery, order);
        finalQuery = applyLimit(finalQuery, limit);

        Cursor cursor = db.rawQuery(finalQuery, null);
        try {
            while (cursor.moveToNext()) {
                String[] row = readArray(cursor);
                items.add(row);
            }
        } finally {
            cursor.close();
            cursor = null;
        }

        return items;
    }

    /**
     * @param query  an existing query
     * @param filter a new filter
     * @return the updated query with the new filter constraint
     */
    protected static String applyFilter(String query, String filter) {
        if (query.contains("WHERE")) {
            query = TextUtils.isEmpty(filter) ? query : query + " AND " + filter;
        } else {
            query = TextUtils.isEmpty(filter) ? query : query + " WHERE " + filter;
        }
        return query;
    }

    /**
     * Applies a limit constraint to an existing SQL query
     *
     * @param query the existing query
     * @param limit the limit constraint
     * @return the updated query with the limit constraint
     */
    protected static String applyLimit(String query, String limit) {
        query = ((limit == null) || (limit.equals(""))) ? query : query + " LIMIT " + limit;
        return query;
    }

    /**
     * Applies an order specification to an existing SQL query
     *
     * @param query the existing query
     * @param order the order specification
     * @return the updated query with the order specification
     */
    protected static String applyOrder(String query, String order) {
        query = ((order == null) || (order.equals(""))) ? query : query + " ORDER BY " + order;
        return query;
    }

    /**
     * @param db the {@link SQLiteDatabase} instance
     * @return the last inserted rowid
     */
    protected static long getLastInsertedId(SQLiteDatabase db) {
        Cursor cursor = db.rawQuery("select last_insert_rowid()", null);
        long id;
        try {
            cursor.moveToNext();
            id = cursor.getLong(0);
        } finally {
            cursor.close();
            cursor = null;
        }
        return id;
    }

    /**
     * Fills a {@link StatementBinder} with some {@link Bean} information
     *
     * @param item      the {@link Bean} to fill the SQL statement
     * @param statement the {@link StatementBinder} object
     */
    protected abstract void fill(E item, StatementBinder statement);

    /**
     * Reads a {@link Bean} from a table row (using a {@link Cursor})
     *
     * @param cursor the {@link Cursor} object to read the object from
     * @return the {@link Bean} corresponding to the row fetched from the cursor
     */
    protected abstract E read(Cursor cursor);

    private String[] readArray(Cursor cursor) {
        String[] row = new String[cursor.getColumnCount()];

        for (int i = 0; i < row.length; i++) {
            row[i] = cursor.getString(i);
        }
        return row;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getLastInsertedId() {
        return getLastInsertedId(this.getSQLiteOpenHelper().getReadableDatabase());
    }
}