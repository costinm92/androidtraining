package com.example.madalincostin.loginregister.activities;

import android.app.ActionBar;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;

import com.android.volley.toolbox.NetworkImageView;
import com.example.madalincostin.loginregister.R;
import com.example.madalincostin.loginregister.persistence.bean.implementation.PhotoBean;
import com.example.madalincostin.loginregister.persistence.bean.implementation.UserFavoritesBean;
import com.example.madalincostin.loginregister.utils.ImageLoaderManager;
import com.example.madalincostin.loginregister.utils.PhotoUtils;
import com.example.madalincostin.loginregister.utils.UserLocalStore;

import java.util.Date;

public class ViewPhotoActivity extends BaseActivity {
    private static final int REMOVE_FROM_FAVORITES = 1;
    private static final int ADD_TO_FAVORITES = 2;

    private UserFavoritesBean userFavoritesBean;
    private PhotoBean favoritePhotoBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTransparentActionBar();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_photo);
        getPhotoFromIntent();
        displayHomeButton();
    }

    /**
     * Styles the ActionBar and set it to be transparent.
     */
    private void setTransparentActionBar() {
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor(getString(R.string.actionbar_color)));
            colorDrawable.setAlpha(96);
            actionBar.setBackgroundDrawable(colorDrawable);
        }
    }

    /**
     * Initializes a PhotoBean with data from the intent and sets the imageUrl on networkImageView.
     */
    private void getPhotoFromIntent() {
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey(PHOTO_IMAGE)) {
            final NetworkImageView networkImageView = (NetworkImageView) findViewById(R.id.imageView);
            favoritePhotoBean = new PhotoBean(getIntent().getExtras().getString(PHOTO_ID),
                    getIntent().getExtras().getString(PHOTO_TITLE),
                    getIntent().getExtras().getString(PHOTO_IMAGE));
            if (UserLocalStore.getInstance().getUserLoggedIn()) {
                userFavoritesBean = new UserFavoritesBean(UserLocalStore.getInstance().getLoggedInUser()
                        .getUsername(), getIntent().getExtras().getString(PHOTO_ID), new Date().getTime());
            }
            networkImageView.setImageUrl(favoritePhotoBean.getUrl(), ImageLoaderManager.getInstance(this).getImageLoader());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case REMOVE_FROM_FAVORITES:
                PhotoUtils.removePhotoFromFavorites(userFavoritesBean);
                showToastMessage(getString(R.string.photo_removed_from_favorites));
                invalidateOptionsMenu();
                break;
            case ADD_TO_FAVORITES:
                PhotoUtils.addNewUserFavoriteBean(favoritePhotoBean);
                showToastMessage(getString(R.string.photo_added_to_favorites));
                invalidateOptionsMenu();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (UserLocalStore.getInstance().getUserLoggedIn()) {
            if (getIntent().getExtras() != null && getIntent().getExtras().containsKey(PHOTO_IMAGE)) {
                if (PhotoUtils.isPhotoFavorite(userFavoritesBean.getPhotoId(), userFavoritesBean.getUsername())) {
                    menu.add(Menu.NONE, REMOVE_FROM_FAVORITES, Menu.NONE, "Remove from favorites");
                } else {
                    menu.add(Menu.NONE, ADD_TO_FAVORITES, Menu.NONE, "Add to favorites");
                }
            }
            getMenuInflater().inflate(R.menu.menu_empty, menu);
        }
        return true;
    }
}
