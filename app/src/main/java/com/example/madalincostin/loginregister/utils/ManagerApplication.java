package com.example.madalincostin.loginregister.utils;

import android.app.Application;

/**
 * Created by Madalin Costin
 * With Android Studio on 2015.
 */
public class ManagerApplication extends Application {
    private static ManagerApplication sInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
    }

    public static ManagerApplication getInstance() {
        if (sInstance == null) {
            synchronized (ManagerApplication.class) {
                if (sInstance == null) {
                    sInstance = new ManagerApplication();
                }
            }
        }
        return sInstance;
    }
}
