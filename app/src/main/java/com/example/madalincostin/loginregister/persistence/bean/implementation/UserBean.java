package com.example.madalincostin.loginregister.persistence.bean.implementation;

import android.support.annotation.NonNull;

import com.example.madalincostin.loginregister.persistence.bean.Bean;
import com.example.madalincostin.loginregister.enums.Gender;

/**
 * Created by Madalin Costin
 * With Android Studio on 2015.
 */
public class UserBean implements Bean, Comparable<UserBean> {
    private String name;
    private String username;
    private String description;
    private String password;
    private Integer age;
    private Gender gender;
    private String preference;

    public UserBean() {
    }

    public UserBean(String name, String username, String password) {
        this.name = name;
        this.username = username;
        this.password = password;
        this.gender = Gender.UNKNOWN;
    }

    @Override
    public Object[] getIdentity() {
        return new Object[]{username};
    }

    @Override
    public boolean requiresEncryption() {
        return false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getPreference() {
        return preference;
    }

    public void setPreference(String preference) {
        this.preference = preference;
    }

    @Override
    public int compareTo(@NonNull UserBean another) {
        if (another.getName() != null) {
            return another.getName().compareTo(this.getName());
        }
        return 1;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((username == null) ? 0 : username.hashCode());
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((password == null) ? 0 : password.hashCode());
        result = prime * result + ((age == null) ? 0 : age.hashCode());
        result = prime * result + ((gender == null) ? 0 : gender.hashCode());
        result = prime * result + ((preference == null) ? 0 : preference.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (((Object) this).getClass() != obj.getClass()) {
            return false;
        }
        UserBean other = (UserBean) obj;
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        if (username == null) {
            if (other.username != null) {
                return false;
            }
        } else if (!username.equals(other.username)) {
            return false;
        }
        if (description == null) {
            if (other.description != null) {
                return false;
            }
        } else if (!description.equals(other.description)) {
            return false;
        }
        if (password == null) {
            if (other.password != null) {
                return false;
            }
        } else if (!password.equals(other.password)) {
            return false;
        }
        if (age == null) {
            if (other.age != null) {
                return false;
            }
        } else if (!age.equals(other.age)) {
            return false;
        }
        if (gender == null) {
            if (other.gender != null) {
                return false;
            }
        } else if (!gender.equals(other.gender)) {
            return false;
        }
        if (preference == null) {
            if (other.preference != null) {
                return false;
            }
        } else if (!preference.equals(other.preference)) {
            return false;
        }
        return true;
    }
}
