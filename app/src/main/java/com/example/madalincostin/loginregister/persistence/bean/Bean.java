package com.example.madalincostin.loginregister.persistence.bean;

/**
 * Super interface for all Bean data-types in the application.
 *
 * @author Marian Lungu
 */
public interface Bean {

    /**
     * The string that will separate fields in the primary key
     */
    String ATTRIBUTE_DETAIL_SEPARATOR = "/";

    /**
     * @return an array of {@link Object} representing the identity of the {@link Bean}
     */
    Object[] getIdentity();

    /**
     * @return true, if the Bean has to be encrypted when saved to the database, false otherwise
     */
    boolean requiresEncryption();
}