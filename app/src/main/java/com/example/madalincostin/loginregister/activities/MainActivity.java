package com.example.madalincostin.loginregister.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.madalincostin.loginregister.R;
import com.example.madalincostin.loginregister.adapters.ViewPagerAdapter;
import com.example.madalincostin.loginregister.fragments.FavoritesFragment;
import com.example.madalincostin.loginregister.fragments.PreferencesFragment;
import com.example.madalincostin.loginregister.fragments.UserDetailFragment;
import com.example.madalincostin.loginregister.utils.UserLocalStore;

public class MainActivity extends BaseActivity {
    private static final int USER_DETAILS_ITEM = 0;
    private static final int USERS_LIST_ITEM = 1;
    private static final int LOGOUT_ITEM = 2;

    private static final int USERS_LIST_ITEM_NORMAL = 0;
    private static final int LOGIN_ITEM = 1;

    private ViewPagerAdapter viewPagerAdapter;
    private ViewPager viewPager;
    private DrawerLayout drawerLayout;
    private ListView drawerList;
    private ActionBarDrawerToggle drawerToggle;

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
        showToastMessage("Wow!!! Do that again!");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case LOGIN_REQUEST_CODE:
                    showToastMessage("Hello " + UserLocalStore.getInstance().getLoggedInUser().getName() + "!");
                    viewPagerAdapter.setIsUserLoggedIn(true);
                    viewPagerAdapter.notifyDataSetChanged();
                    setDrawerItems();
                    invalidateOptionsMenu();
                    break;
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle(R.string.title_activity_view_users);

        viewPager = (ViewPager) findViewById(R.id.pager);
        viewPagerAdapter = new ViewPagerAdapter(getFragmentManager());
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }

            @Override
            public void onPageSelected(int position) {
                if (position == ViewPagerAdapter.USER_PREFERENCES_PAGE) {
                    Fragment fragment = viewPagerAdapter.getRegisteredFragment(position);
                    if (fragment instanceof PreferencesFragment) {
                        ((PreferencesFragment) fragment).initializeElements();
                    }
                } else if (position == ViewPagerAdapter.USER_DETAILS_PAGE) {
                    Fragment fragment = viewPagerAdapter.getRegisteredFragment(position);
                    if (fragment instanceof UserDetailFragment) {
                        ((UserDetailFragment) fragment).initializeElements(UserLocalStore.getInstance().getLoggedInUser());
                    }
                } else if (position == ViewPagerAdapter.USER_FAVORITES_PAGE) {
                    Fragment fragment = viewPagerAdapter.getRegisteredFragment(position);
                    if (fragment instanceof FavoritesFragment) {
                        ((FavoritesFragment) fragment).initializeElements();
                    }
                }
            }
        });
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setOffscreenPageLimit(ViewPagerAdapter.ITEMS_NUMBER_LOGGEDIN - 1);

        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.drawable.ic_navigation_drawer, R.string.drawer_open, R.string.drawer_close);
        drawerLayout.setDrawerListener(drawerToggle);
        drawerList = (ListView) findViewById(R.id.left_drawer);
        drawerList.setOnItemClickListener(new DrawerItemClickListener());
        setDrawerItems();
    }

    /**
     * Initializes the Drawer items based on the user status: logged id or not.
     */
    private void setDrawerItems() {
        String[] titles;
        if (UserLocalStore.getInstance().getUserLoggedIn()) {
            titles = new String[]{"My Profile", "Users List", "Logout"};
        } else {
            titles = new String[]{"Users List", "Login"};
        }
        drawerList.setAdapter(new ArrayAdapter<>(this, R.layout.drawer_list_item, titles));
    }

    @Override
    protected void onStart() {
        super.onStart();
        displayPages();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    /**
     * Sets the View pager adapter isUserLoggedIn property based on the user status: logged id or not.
     */
    private void displayPages() {
        if (UserLocalStore.getInstance().getUserLoggedIn()) {
            viewPagerAdapter.setIsUserLoggedIn(true);
            viewPagerAdapter.notifyDataSetChanged();
        } else {
            viewPagerAdapter.setIsUserLoggedIn(false);
            viewPagerAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return drawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }

    @Override
    protected void logout() {
        int currentItem = viewPager.getCurrentItem();
        super.logout();
        viewPagerAdapter.setIsUserLoggedIn(false);
        viewPagerAdapter.notifyDataSetChanged();
        if (currentItem >= ViewPagerAdapter.ITEMS_NUMBER_NORMAL) {
            viewPager.setCurrentItem(ViewPagerAdapter.ITEMS_NUMBER_NORMAL - 1);
        } else {
            viewPager.setCurrentItem(currentItem);
        }
        invalidateOptionsMenu();
        setDrawerItems();
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(drawerList);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, @NonNull KeyEvent e) {
        if (keyCode == KeyEvent.KEYCODE_MENU) {
            if (!drawerLayout.isDrawerOpen(drawerList)) {
                drawerLayout.openDrawer(drawerList);
            }
            return true;
        }
        return super.onKeyDown(keyCode, e);
    }

    /**
     * Listener for Drawer item click. It starts a new activity with the selected option.
     */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if (UserLocalStore.getInstance().getUserLoggedIn()) {
                switch (position) {
                    case USER_DETAILS_ITEM:
                        Intent intent = new Intent(MainActivity.this, UserDetailsActivity.class);
                        intent.putExtra(BaseActivity.USER_NAME, UserLocalStore.getInstance().getLoggedInUser().getUsername());
                        startActivity(intent);
                        break;
                    case USERS_LIST_ITEM:
                        startActivity(new Intent(MainActivity.this, UsersListActivity.class));
                        break;
                    case LOGOUT_ITEM:
                        logout();
                        break;
                }
            } else {
                switch (position) {
                    case USERS_LIST_ITEM_NORMAL:
                        startActivity(new Intent(MainActivity.this, UsersListActivity.class));
                        break;
                    case LOGIN_ITEM:
                        login();
                        break;
                }
            }
            drawerLayout.closeDrawer(drawerList);
        }
    }
}