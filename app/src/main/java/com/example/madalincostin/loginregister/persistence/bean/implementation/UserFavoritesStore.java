package com.example.madalincostin.loginregister.persistence.bean.implementation;

import com.example.madalincostin.loginregister.persistence.ManagerDBOpenHelper;
import com.example.madalincostin.loginregister.persistence.bean.BeanFilter;
import com.example.madalincostin.loginregister.persistence.bean.BeanStoreImpl;
import com.example.madalincostin.loginregister.persistence.dao.DAO;
import com.example.madalincostin.loginregister.persistence.dao.implementation.UserFavoritesDAO;
import com.example.madalincostin.loginregister.utils.ManagerApplication;

import java.util.List;

/**
 * Created by Madalin Costin
 * With Android Studio on 2015.
 */
public class UserFavoritesStore extends BeanStoreImpl<UserFavoritesBean> {
    private static UserFavoritesStore sInstance;

    public static UserFavoritesStore getInstance() {
        if (sInstance == null) {
            synchronized (UserFavoritesStore.class) {
                if (sInstance == null) {
                    sInstance = new UserFavoritesStore();
                }
            }
        }
        return sInstance;
    }

    @Override
    public DAO<UserFavoritesBean> getDAO() {
        return new UserFavoritesDAO(ManagerDBOpenHelper.getInstance(ManagerApplication.getInstance()));
    }

    public List<UserFavoritesBean> getAllUsersFavorites(final String username) {
        return super.getItems(new BeanFilter<UserFavoritesBean>() {
            @Override
            public boolean accept(UserFavoritesBean bean) {
                return bean.getUsername().equals(username);
            }
        }, null);
    }

    public boolean isPhotoInUserList(final String photoId, final String username) {
        List<UserFavoritesBean> userFavoritesBeans = super.getItems(new BeanFilter<UserFavoritesBean>() {
            @Override
            public boolean accept(UserFavoritesBean bean) {
                return bean.getUsername().equals(username) && bean.getPhotoId().equals(photoId);
            }
        }, null);
        return userFavoritesBeans.size() > 0;
    }

    public UserFavoritesBean getPhoto(final String photoId, final String username) {
        List<UserFavoritesBean> userFavoritesBeans = super.getItems(new BeanFilter<UserFavoritesBean>() {
            @Override
            public boolean accept(UserFavoritesBean bean) {
                return bean.getUsername().equals(username) && bean.getPhotoId().equals(photoId);
            }
        }, null);
        if (userFavoritesBeans != null && !userFavoritesBeans.isEmpty()) {
            return userFavoritesBeans.get(0);
        } else {
            return null;
        }
    }
}
