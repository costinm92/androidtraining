package com.example.madalincostin.loginregister.activities;

import android.app.Activity;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.madalincostin.loginregister.R;
import com.example.madalincostin.loginregister.fragments.DescriptionFragment;
import com.example.madalincostin.loginregister.persistence.bean.implementation.UserBean;
import com.example.madalincostin.loginregister.persistence.bean.implementation.UserStore;
import com.example.madalincostin.loginregister.enums.Gender;
import com.example.madalincostin.loginregister.utils.UserLocalStore;

public class EditUserActivity extends BaseActivity implements DescriptionFragment.OnDataPassListener {
    private TextView vUsername;
    private EditText etAge;
    private EditText etName;
    private RadioButton rMale;
    private RadioButton rFemale;
    private Spinner preferenceSpinner;
    private FragmentManager fm = getFragmentManager();
    private UserBean user;

    /**
     * Listener for Edit button click. It updates the user variable fields and updates the
     * database with its values.
     */
    private View.OnClickListener onEditClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            user.setName(etName.getText().toString());
            user.setUsername(vUsername.getText().toString());
            if (!etAge.getText().toString().isEmpty()) {
                user.setAge(Integer.parseInt(etAge.getText().toString()));
            }
            if (rMale.isChecked()) {
                user.setGender(Gender.MALE);
            } else if (rFemale.isChecked()) {
                user.setGender(Gender.FEMALE);
            } else {
                user.setGender(Gender.UNKNOWN);
            }
            if (!preferenceSpinner.getSelectedItem().toString().equals("None")) {
                user.setPreference(preferenceSpinner.getSelectedItem().toString());
            } else {
                user.setPreference("");
            }
            UserStore.getInstance().update(user);
            UserLocalStore.getInstance().setLoggedInUser(user);
            setResult(Activity.RESULT_OK);
            finish();
        }
    };

    /**
     * Listener for Edit Description button click. It initializes a DescriptionFragment
     * and sends the user description to it as a bundle argument.
     */
    private View.OnClickListener onEditDescriptionClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Bundle bundle = new Bundle();
            bundle.putString(DESCRIPTION_KEY, user.getDescription());
            DescriptionFragment descriptionFragment = new DescriptionFragment();
            descriptionFragment.setArguments(bundle);
            descriptionFragment.show(fm, "Edit Description");
        }
    };

    /**
     * Initializes the user variable from the user passed in intent.
     */
    private void getUserFromIntent() {
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey(USER_NAME)) {
            user = UserStore.getInstance().getUser(getIntent().getExtras().getString(USER_NAME));
        }
    }

    /**
     * Initializes a spinner(dropdown) and sets its position.
     * @param preference is the position to set on spinner.
     */
    private void addSpinnerAdapterAndSetPosition(String preference) {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.preferences_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        preferenceSpinner.setAdapter(adapter);
        if (preference != null) {
            int spinnerPosition = adapter.getPosition(preference);
            preferenceSpinner.setSelection(spinnerPosition);
        }
    }

    /**
     * Initializes the UI fields with data from the user.
     */
    private void initializeUser() {
        if (user != null) {
            etName.setText(user.getName());
            if (user.getAge() != -1) {
                etAge.setText(user.getAge() + "");
            }
            vUsername.setText(user.getUsername());
            rMale.setChecked(user.getGender().equals(Gender.MALE));
            rFemale.setChecked(user.getGender().equals(Gender.FEMALE));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDataPass(String data) {
        user.setDescription(data);
        showToastMessage("Description successfully changed! New description: " + data);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user);

        etName = (EditText) findViewById(R.id.etName);
        etAge = (EditText) findViewById(R.id.etAge);
        vUsername = (TextView) findViewById(R.id.vUsername);
        rMale = (RadioButton) findViewById(R.id.rMale);
        rFemale = (RadioButton) findViewById(R.id.rFemale);
        preferenceSpinner = (Spinner) findViewById(R.id.preferencesSpinner);
        Button iEdit = (Button) findViewById(R.id.bEdit);
        Button bEditDescription = (Button) findViewById(R.id.bEditDescription);
        bEditDescription.setOnClickListener(onEditDescriptionClick);
        iEdit.setOnClickListener(onEditClick);

        getUserFromIntent();
        initializeUser();
        addSpinnerAdapterAndSetPosition(user.getPreference());
        displayHomeButton();
    }
}
