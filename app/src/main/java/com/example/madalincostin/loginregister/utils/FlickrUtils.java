package com.example.madalincostin.loginregister.utils;

/**
 * Created by Madalin Costin
 * With Android Studio on 2015.
 */
public class FlickrUtils {
    public static final String FLICKR_API_KEY = "989e5b5a909df274932a4e6327a00648";

    public static String getImageUrl(String farm, String server, String id, String secret) {
        String unformattedResponse = "https://farm" + farm + ".staticflickr.com/"
                + server + "/" + id + "_" + secret + ".jpg";
        return unformattedResponse.replace("\"", "");
    }
}
