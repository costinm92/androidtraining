package com.example.madalincostin.loginregister.activities;

import android.app.FragmentManager;
import android.os.Bundle;
import android.view.MenuItem;

import com.example.madalincostin.loginregister.R;
import com.example.madalincostin.loginregister.fragments.UsersListFragment;

public class UsersListActivity extends BaseActivity {
    /**
     * Creates a new UsersListFragment.
     * @return the newly created UsersListFragment.
     */
    private UsersListFragment getUsersListFragment() {
        return new UsersListFragment();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users_list);

        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, getUsersListFragment())
                .commit();
        displayHomeButton();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}