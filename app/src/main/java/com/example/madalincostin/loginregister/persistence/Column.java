package com.example.madalincostin.loginregister.persistence;

import java.util.ArrayList;
import java.util.List;

/**
 * This type represents a column in the definition of a table that will be stored in the database.
 *
 * @author Marian Lungu
 */
public class Column {

    public static final String TYPE_INTEGER = "INTEGER";
    public static final String TYPE_LONG = "LONG";
    public static final String TYPE_REAL = "REAL";
    public static final String TYPE_TEXT = "TEXT";
    public static final String TYPE_BLOB = "LONGBLOB";

    public static final String CONSTRAINT_PRIMARY_KEY = "PRIMARY KEY";
    public static final String CONSTRAINT_AUTOINCREMENT = "AUTOINCREMENT";

    private String mName;
    private String mType;
    private List<String> mConstrainList;
    private String mConstraints;

    /**
     * Public constructor
     *
     * @param name        the name of the column
     * @param type        the data-type that will hold
     * @param constraints the list of constraints
     */
    public Column(String name, String type, String... constraints) {
        this.mName = name;
        this.mType = type;
        this.mConstrainList = new ArrayList<>();
        for (String constraint : constraints) {
            addConstraint(constraint);
        }
    }

    /**
     * Public constructor with no constraints
     *
     * @param name the name of the column
     * @param type he data-type that will hold
     */
    public Column(String name, String type) {
        this(name, type, "");
    }

    /**
     * Adds a constraint to the {@link Column} definition
     *
     * @param constraint the new constraint to be added
     * @return always true
     */
    public synchronized boolean addConstraint(String constraint) {
        mConstraints = null;
        return mConstrainList.add(constraint);
    }

    /**
     * Removes a constraint from this {@link Column} definition
     *
     * @param constraint the contraint to be removed
     * @return true if the constraints list was modified by this operation, false otherwise.
     */
    public synchronized boolean removeConstraint(String constraint) {
        mConstraints = null;
        return mConstrainList.remove(constraint);
    }

    /**
     * @param constraint the constraint to be looked up
     * @return true, if the {@link Column} already has been configured with this constraing, false otherwise
     */
    public boolean hasConstraint(String constraint) {
        return mConstrainList.contains(constraint);
    }

    /**
     * @return the name of the {@link Column}
     */
    public String getName() {
        return mName;
    }

    /**
     * @return the type of the {@link Column}
     */
    public String getType() {
        return mType;
    }

    /**
     * @return the constraints list specified for this {@link Column}
     */
    public synchronized String getConstraints() {
        if (mConstraints == null) {
            mConstraints = "";
            String[] constraintsArray = mConstrainList.toArray(new String[0]);
            for (int i = 0; i < constraintsArray.length; i++) {
                String separator = (i == constraintsArray.length - 1) ? "" : " ";
                this.mConstraints += constraintsArray[i] + separator;
            }
        }
        return mConstraints;
    }

    /**
     * @param value the value for the currect {@link Column}
     * @return a filter query that will match the current {@link Column} for the given value
     */
    public String getFilter(Object value) {
        if (value == null) {
            return mName + " IS NULL";
        } else if (TYPE_INTEGER.equals(mType) || TYPE_LONG.equals(mType) || TYPE_REAL.equals(mType)) {
            return mName + " = " + value.toString();
        } else if (TYPE_TEXT.equals(mType)) {
            return mName + " = '" + value.toString() + "'";
        } else {
            return null;
        }
    }
}
