package com.example.madalincostin.loginregister.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.example.madalincostin.loginregister.R;
import com.example.madalincostin.loginregister.activities.BaseActivity;

public class DescriptionFragment extends DialogFragment {
    private OnDataPassListener dataPasser;
    private EditText etDescription;

    @Override
    public void onAttach(Activity a) {
        super.onAttach(a);
        dataPasser = (OnDataPassListener) a;
    }

    public void passData(String data) {
        dataPasser.onDataPass(data);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.fragment_description, null);
        etDescription = (EditText) view.findViewById(R.id.etDescription);
        String description = getArguments().getString(BaseActivity.DESCRIPTION_KEY);
        etDescription.setText(description);
        builder.setTitle("Edit description");
        builder.setView(view)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        passData(etDescription.getText().toString());
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        DescriptionFragment.this.getDialog().cancel();
                    }
                });
        return builder.create();
    }

    public interface OnDataPassListener {
        void onDataPass(String data);
    }
}