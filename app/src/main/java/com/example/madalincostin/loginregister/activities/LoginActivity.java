package com.example.madalincostin.loginregister.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.madalincostin.loginregister.R;
import com.example.madalincostin.loginregister.persistence.bean.implementation.UserBean;
import com.example.madalincostin.loginregister.persistence.bean.implementation.UserStore;
import com.example.madalincostin.loginregister.utils.UserLocalStore;

public class LoginActivity extends BaseActivity {
    private EditText etUsername;
    private EditText etPassword;
    private Button bLogin;

    /**
     * Listener for Login button click. It checks if the username and password are valid.
     * If they are valid, it sets the result as RESULT_OK and destroys the activity.
     * If they are not valid, it shows an informative toast message.
     */
    private View.OnClickListener onLoginClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            UserStore userStore = UserStore.getInstance();
            UserBean validUser = userStore.getUser(etUsername.getText().toString());
            if (validUser != null && validUser.getPassword().equals(etPassword.getText().toString())) {
                UserLocalStore.getInstance().setLoggedInUser(validUser);
                setResult(Activity.RESULT_OK);
                finish();
            } else {
                showToastMessage(getString(R.string.invalid_credentials));
            }
        }
    };

    /**
     * Listener for Register button click. It starts a new activity.
     */
    private View.OnClickListener onRegisterClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivityForResult(new Intent(LoginActivity.this, RegisterActivity.class), REGISTER_REQUEST_CODE);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etUsername = (EditText) findViewById(R.id.etUsername);
        etPassword = (EditText) findViewById(R.id.etPassword);
        bLogin = (Button) findViewById(R.id.bLogin);
        TextView tvRegisterLink = (TextView) findViewById(R.id.tvRegisterLink);

        bLogin.setOnClickListener(onLoginClick);
        tvRegisterLink.setOnClickListener(onRegisterClick);

        TextView.OnEditorActionListener editorActionListener = new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (v == etPassword) {
                        bLogin.callOnClick();
                    }
                    return true;
                }
                return false;
            }
        };

        etUsername.setOnEditorActionListener(editorActionListener);
        etPassword.setOnEditorActionListener(editorActionListener);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REGISTER_REQUEST_CODE:
                    showToastMessage("User " + data.getExtras().getString("name") + " successfully created!");
                    break;
            }
        }
    }
}
