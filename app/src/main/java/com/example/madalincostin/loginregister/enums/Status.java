package com.example.madalincostin.loginregister.enums;

/**
 * Created by Madalin Costin
 * With Android Studio on 2015.
 */
public enum Status {
    SUCCESS(0),
    ERROR(1);
    int value;
    Status(int value) {
        this.value = value;
    }
}
