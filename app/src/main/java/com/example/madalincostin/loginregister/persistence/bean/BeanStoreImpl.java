package com.example.madalincostin.loginregister.persistence.bean;

import com.example.madalincostin.loginregister.persistence.dao.DAO;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Base class designed to allow easy heap and eventually persistence storage of
 * Bean structures.
 * <p/>
 * A BeanStore will assure persistence storage if the getDAO implementation
 * returns a properly functioning DAO instance and nobody else is writing into
 * the database.<br>
 *
 * @param <E> the Bean type for which this store assures storage
 */
public abstract class BeanStoreImpl<E extends Bean> implements BeanStore<E> {

    protected final Map<String, E> mMemCache;

    /**
     * Lock object used to lock storage operations
     */
    private static final Object sLock = new Object();

    /**
     * The first invocation can be a bit lengthy as all the items from the
     * storage are read. You may want to invoke this earlier than the last
     * possible moment...
     */
    protected BeanStoreImpl() {
        mMemCache = Collections.synchronizedMap(new LinkedHashMap<String, E>());
        sync();
    }

    /**
     * @return the {@link DAO} instance which will handle the database access operations for this {@link BeanStore}
     */
    public abstract DAO<E> getDAO();

    /**
     * {@inheritDoc}
     */
    @Override
    public int count() {
        return mMemCache.size();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E getItem(Object... identity) {
        if (mMemCache.get(mergeIds(identity)) == null) {
            sync(identity);
        }
        return mMemCache.get(mergeIds(identity));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<E> getItems(BeanFilter<E> filter, Comparator<E> comparator, int pageSize, int page) {
        List<E> filtered = new ArrayList<E>();
        Entry<String, E> item;
        sync(pageSize, page);

        try {
            if (!mMemCache.isEmpty()) {
                ArrayList<Entry<String, E>> allItems = new ArrayList<Entry<String, E>>(mMemCache.entrySet());
                for (Iterator<Entry<String, E>> i = allItems.iterator(); i.hasNext(); ) {
                    item = i.next();

                    if ((filter == null) || (filter.accept(item.getValue()))) {
                        filtered.add(item.getValue());
                    }
                }

                if (comparator != null) {
                    Collections.sort(filtered, comparator);
                }
            }
        } catch (Exception e) {
        }
        return filtered;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<E> getItems(BeanFilter<E> filter, Comparator<E> comparator) {
        List<E> filtered = new ArrayList<E>();
        Entry<String, E> item;

        if (mMemCache.isEmpty()) {
            sync();
        }

        try {
            if (!mMemCache.isEmpty()) {
                ArrayList<Entry<String, E>> allItems = new ArrayList<Entry<String, E>>(mMemCache.entrySet());
                for (Iterator<Entry<String, E>> i = allItems.iterator(); i.hasNext(); ) {
                    item = i.next();

                    if ((filter == null) || (filter.accept(item.getValue()))) {
                        filtered.add(item.getValue());
                    }
                }

                if (comparator != null) {
                    Collections.sort(filtered, comparator);
                }
            }
        } catch (Exception e) {
        }
        return filtered;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<E> getItems(BeanFilter<E> filter, Comparator<E> comparator, int maxResults) {
        List<E> filtered = new ArrayList<E>();
        Entry<String, E> item;

        if (mMemCache.isEmpty()) {
            sync();
        }

        try {
            if (!mMemCache.isEmpty()) {
                ArrayList<Entry<String, E>> allItems = new ArrayList<Entry<String, E>>(mMemCache.entrySet());
                for (Iterator<Entry<String, E>> i = allItems.iterator(); (i.hasNext() && filtered.size() < maxResults); ) {
                    item = i.next();

                    if ((filter == null) || (filter.accept(item.getValue()))) {
                        filtered.add(item.getValue());
                    }
                }

                if (comparator != null) {
                    Collections.sort(filtered, comparator);
                }
            }
        } catch (Exception e) {
        }
        return filtered;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void add(E item, boolean updateIfExists) {
        sync(item.getIdentity());

        synchronized (mMemCache) {
            String id = mergeIds(item.getIdentity());

            if (mMemCache.get(id) == null || updateIfExists) {
                mMemCache.put(id, item);
            }

            if (getDAO() != null) {
                synchronized (sLock) {
                    getDAO().insert(item, updateIfExists);
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addAll(List<E> addedItems, boolean updateIfExists) {
        synchronized (mMemCache) {
            for (E item : addedItems) {
                String id = mergeIds(item.getIdentity());

                if (mMemCache.get(id) == null || updateIfExists) {
                    mMemCache.put(id, item);
                }
            }
            if (getDAO() != null && addedItems.size() > 0) {
                synchronized (sLock) {
                    getDAO().insert(addedItems, updateIfExists);
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean update(E item) {
        synchronized (mMemCache) {
            String id = mergeIds(item.getIdentity());

            if (mMemCache.get(id) == null) {
                sync(item.getIdentity());
            }

            if (mMemCache.get(id) != null) {
                mMemCache.put(id, item);

                if (getDAO() != null) {
                    synchronized (sLock) {
                        getDAO().update(item);
                    }
                }
                return true;
            }
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateAll(List<E> updateItems) {
        for (Iterator<E> i = updateItems.iterator(); i.hasNext(); ) {
            update(i.next());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E remove(E item) {
        E result;
        synchronized (mMemCache) {
            result = mMemCache.remove(mergeIds(item.getIdentity()));

            if (getDAO() != null) {
                synchronized (sLock) {
                    getDAO().delete(item);
                }
            }
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeAll(E[] items) {
        synchronized (mMemCache) {
            if (getDAO() != null) {
                synchronized (sLock) {
                    getDAO().deleteAll(items);
                }
            }

            for (E item : items) {
                mMemCache.remove(mergeIds(item.getIdentity()));
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void replaceAll(List<E> items) {
        synchronized (this.mMemCache) {
            clear();
            addAll(items, false);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clear() {
        synchronized (mMemCache) {
            mMemCache.clear();

            if (getDAO() != null) {
                synchronized (sLock) {
                    getDAO().deleteAll();
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onStorageModified() {
        mMemCache.clear();
        sync();
    }

    /**
     * Re-read all items from storage
     */
    protected void sync() {
        if (getDAO() != null) {
            List<E> items = getDAO().list();

            synchronized (mMemCache) {
                for (E item : items) {
                    this.mMemCache.put(mergeIds(item.getIdentity()), item);
                }
            }
        }
    }

    /**
     * Re-read certain items from storage
     */
    protected void sync(int pageSize, int page) {
        E item;

        if (getDAO() != null) {
            List<E> items = getDAO().list(pageSize, page);

            synchronized (mMemCache) {
                for (Iterator<E> i = items.iterator(); i.hasNext(); ) {
                    item = i.next();
                    this.mMemCache.put(mergeIds(item.getIdentity()), item);
                }
            }
        }
    }

    /**
     * Re-read the specified item from storage
     */
    protected void sync(Object[] identity) {
        if (getDAO() != null) {
            E item = getDAO().get(identity);

            if (item != null) {
                this.mMemCache.put(mergeIds(item.getIdentity()), item);
            }
        }
    }

    private static String mergeIds(Object[] ids) {
        StringBuilder buffer = new StringBuilder();

        for (Object id : ids) {
            buffer.append(id);
            buffer.append(Bean.ATTRIBUTE_DETAIL_SEPARATOR);
        }

        return ids.length > 0 ? buffer.substring(0, buffer.length() - 1) : "";
    }
}