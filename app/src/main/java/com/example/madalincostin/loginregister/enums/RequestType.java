package com.example.madalincostin.loginregister.enums;

/**
 * Created by Madalin Costin
 * With Android Studio on 2015.
 */
public enum RequestType {
    PREFERENCES(0),
    SEARCH(1);
    int value;
    RequestType(int value) {
        this.value = value;
    }
}
