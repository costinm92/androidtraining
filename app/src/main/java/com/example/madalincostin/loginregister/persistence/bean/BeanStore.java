package com.example.madalincostin.loginregister.persistence.bean;

import java.util.Comparator;
import java.util.List;

/**
 * Generic interface for all the operations that will be available on a Store for a specific {@link Bean} class
 *
 * @param <E> the sub-class of {@link Bean} that this BeanStore will be handling
 * @author Marian Lungu
 */
public interface BeanStore<E extends Bean> {

    /**
     * @return the number of items in storage
     */
    int count();

    /**
     * Get the item with the specified identity from storage if exists
     *
     * @param identity identity to search for
     * @return item null if not found
     */
    E getItem(Object... identity);

    /**
     * Get a page of filtered items from storage sorted by the specified
     * comparator.
     *
     * @param filter       applied before sorting and paging; if null, all items from
     *                     storage are returned
     * @param comparator   applied after filtering but before paging; can be null
     * @param countPerPage maximum number of items returned/items per page
     * @param page         valid values are: [0, floor(count() / countPerPage)]
     * @return List of items
     */
    List<E> getItems(BeanFilter<E> filter, Comparator<E> comparator, int countPerPage, int page);

    /**
     * Get a list of filtered items from storage sorted by the specified
     * comparator.
     *
     * @param filter     applied before sorting; if null, all items from storage are
     *                   returned
     * @param comparator applied after filtering but before paging; can be null
     * @return List of items
     */
    List<E> getItems(BeanFilter<E> filter, Comparator<E> comparator);

    /**
     * Get a list of filtered items having the maximum dimension specified by maxResults parameter from storage sorted
     * by the specified comparator.
     *
     * @param filter     applied before sorting; if null, all items from storage are returned
     * @param comparator applied after filtering but before paging; can be null
     * @return List of items
     */
    List<E> getItems(BeanFilter<E> filter, Comparator<E> comparator, int maxResults);

    /**
     * Add the specified item to the storage
     *
     * @param item           the item to be added
     * @param updateIfExists If true and the specified item is already in the storage, it's
     *                       attributes will be updated. If false and the specified item is
     *                       already in the storage, this operation will have no effect.
     */
    void add(E item, boolean updateIfExists);

    /**
     * Add all specified items to the storage
     *
     * @param addedItems     items to be added
     * @param updateIfExists If set to true, new items are added to the table, items that
     *                       already exist will be updated. If set to false, new items are
     *                       added to the table but existing items are not modified.
     */
    void addAll(List<E> addedItems, boolean updateIfExists);

    /**
     * Update an item in the storage. This method will only have effect if there
     * already is an item with the same identity in the storage. The specified
     * item must have it's identity set.
     *
     * @param item the item to be updated
     * @return true if item was found, false otherwise
     */
    boolean update(E item);

    /**
     * Update all items in storage. Items that are not already in the storage
     * will be ignored.
     */
    void updateAll(List<E> updateItems);

    /**
     * Remove the specified item from storage
     *
     * @param item the item to be removed
     * @return the removed item or null if not found
     */
    E remove(E item);

    /**
     * Remove the specified items from storage
     *
     * @param items the items to be removed
     */
    void removeAll(E[] items);

    /**
     * Clear storage and add the specified items
     *
     * @param items the items to be added
     */
    void replaceAll(List<E> items);

    /**
     * Clear all items from storage
     */
    void clear();

    /**
     * Useful when the database is changed outside the application and the cache
     * should be refreshed<br>
     * TODO should probably be removed before release, currently only used for
     * testing purposes and on rooted phone
     */
    void onStorageModified();
}