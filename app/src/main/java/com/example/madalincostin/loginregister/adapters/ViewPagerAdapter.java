package com.example.madalincostin.loginregister.adapters;

import android.app.Fragment;
import android.app.FragmentManager;

import com.example.madalincostin.loginregister.fragments.FavoritesFragment;
import com.example.madalincostin.loginregister.fragments.PreferencesFragment;
import com.example.madalincostin.loginregister.fragments.SearchPhotosFragment;
import com.example.madalincostin.loginregister.fragments.UserDetailFragment;
import com.example.madalincostin.loginregister.fragments.UsersListFragment;
import com.example.madalincostin.loginregister.utils.UserLocalStore;

/**
 * Created by Madalin Costin
 * With Android Studio on 2015.
 */
public class ViewPagerAdapter extends RegisteredFragmentsPagerAdapter {
    public static final int USERS_LIST_PAGE = 0;
    public static final int USER_PHOTO_SEARCH = 1;
    public static final int USER_DETAILS_PAGE = 2;
    public static final int USER_PREFERENCES_PAGE = 3;
    public static final int USER_FAVORITES_PAGE = 4;

    public static final int ITEMS_NUMBER_LOGGEDIN = 5;
    public static final int ITEMS_NUMBER_NORMAL = 2;

    private boolean isUserLoggedIn;

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public void setIsUserLoggedIn(boolean isUserLoggedIn) {
        this.isUserLoggedIn = isUserLoggedIn;
    }

    @Override
    public Fragment getItem(int i) {
        Fragment fragment = null;
        if (isUserLoggedIn) {
            switch (i) {
                case USERS_LIST_PAGE:
                    fragment = new UsersListFragment();
                    break;
                case USER_PHOTO_SEARCH:
                    fragment = new SearchPhotosFragment();
                    break;
                case USER_DETAILS_PAGE:
                    fragment = UserDetailFragment.newInstance(UserLocalStore.getInstance().getLoggedInUser().getUsername());
                    break;
                case USER_PREFERENCES_PAGE:
                    fragment = new PreferencesFragment();
                    break;
                case USER_FAVORITES_PAGE:
                    fragment = new FavoritesFragment();
                    break;
            }
        } else {
            switch (i) {
                case USERS_LIST_PAGE:
                    fragment = new UsersListFragment();
                    break;
                case USER_PHOTO_SEARCH:
                    fragment = new SearchPhotosFragment();
                    break;
            }
        }
        return fragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String pageTitle = "";
        if (isUserLoggedIn) {
            switch (position) {
                case USERS_LIST_PAGE:
                    pageTitle = "Users List";
                    break;
                case USER_PHOTO_SEARCH:
                    pageTitle = "Search photos";
                    break;
                case USER_DETAILS_PAGE:
                    pageTitle = "My Profile";
                    break;
                case USER_PREFERENCES_PAGE:
                    pageTitle = "My Preferences";
                    break;
                case USER_FAVORITES_PAGE:
                    pageTitle = "My Favorites";
                    break;
            }
        } else {
            switch (position) {
                case USERS_LIST_PAGE:
                    pageTitle = "Users List";
                    break;
                case USER_PHOTO_SEARCH:
                    pageTitle = "Search photos";
                    break;
            }
        }
        return pageTitle;
    }

    @Override
    public int getCount() {
        if (isUserLoggedIn) {
            return ITEMS_NUMBER_LOGGEDIN;
        } else {
            return ITEMS_NUMBER_NORMAL;
        }
    }
}