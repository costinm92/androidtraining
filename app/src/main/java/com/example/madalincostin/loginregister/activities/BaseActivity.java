package com.example.madalincostin.loginregister.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.widget.Toast;

import com.example.madalincostin.loginregister.R;
import com.example.madalincostin.loginregister.utils.UserLocalStore;

public abstract class BaseActivity extends Activity {
    public static final String USER_NAME = "username";
    public static final String DESCRIPTION_KEY = "description";
    public static final String PHOTO_ID = "photoId";
    public static final String PHOTO_TITLE = "photoTitle";
    public static final String PHOTO_IMAGE = "photoImage";
    public static final String DEFAULT_VALUE = "Unknown";

    protected static final int LOGIN_REQUEST_CODE = 1;
    protected static final int EDIT_REQUEST_CODE = 2;
    protected static final int REGISTER_REQUEST_CODE = 3;

    /**
     * Shows a toast message.
     * @param toastMessage is the message to be shown.
     */
    protected void showToastMessage(String toastMessage) {
        Toast.makeText(this, toastMessage, Toast.LENGTH_LONG).show();
    }

    /**
     * Start the login activity.
     */
    protected void login() {
        startActivityForResult(new Intent(this, LoginActivity.class), LOGIN_REQUEST_CODE);
    }

    /**
     * Logs out an user and shows a toast message.
     */
    protected void logout() {
        UserLocalStore.getInstance().clearUserData();
        this.invalidateOptionsMenu();
        showToastMessage(getString(R.string.logout_success));
    }

    /**
     * Display the home button on action bar.
     */
    protected void displayHomeButton() {
        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    /**
     * Checks if the user passed in the intent is logged in.
     * @return boolean: true if the user is logged in and false if not.
     */
    public boolean isCurrentUserLoggedIn() {
        if (getIntent().getExtras() == null || !getIntent().getExtras().containsKey(USER_NAME)
                || !UserLocalStore.getInstance().getUserLoggedIn()) {
            return false;
        } else if (getIntent().getExtras().getString(USER_NAME).equals(UserLocalStore.getInstance().getLoggedInUser().getUsername())) {
            return true;
        }
        return false;
    }
}
