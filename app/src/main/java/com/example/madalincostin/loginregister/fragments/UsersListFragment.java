package com.example.madalincostin.loginregister.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.madalincostin.loginregister.R;
import com.example.madalincostin.loginregister.activities.BaseActivity;
import com.example.madalincostin.loginregister.activities.UserDetailsActivity;
import com.example.madalincostin.loginregister.persistence.bean.implementation.UserBean;
import com.example.madalincostin.loginregister.persistence.bean.implementation.UserStore;

import java.util.List;

public class UsersListFragment extends Fragment {
    private ListView listView;

    /**
     * Listener for the long item click. It converts the item to an UserBean and displays
     * a toast message with the user's name and age.
     */
    private AdapterView.OnItemLongClickListener onItemLongClickListener = new AdapterView.OnItemLongClickListener() {
        @Override
        public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
            UserBean user = (UserBean) parent.getItemAtPosition(position);
            String toastMessage = user.getName() + ", " + user.getAge() + " years old!";
            Toast.makeText(listView.getContext(), toastMessage, Toast.LENGTH_LONG).show();
            return true;
        }
    };

    /**
     * Listener for the item click. It calls the redirectToUserPage function with selected
     * item converted to UserBean as a parameter.
     */
    private AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            redirectToUserPage((UserBean) parent.getItemAtPosition(position));
        }
    };

    /**
     * Starts a new intent with the username as a parameter and then starts a new activity
     * with the newly created intent.
     * @param user is the user which provides the username to be set on the intent.
     */
    private void redirectToUserPage(UserBean user) {
        Intent intent = new Intent(getActivity(), UserDetailsActivity.class);
        intent.putExtra(BaseActivity.USER_NAME, user.getUsername());
        startActivity(intent);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_users_list, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        listView = (ListView) view.findViewById(R.id.listView);
        listView.setOnItemLongClickListener(onItemLongClickListener);
        listView.setOnItemClickListener(onItemClickListener);
    }

    @Override
    public void onStart() {
        super.onStart();
        displayUsers();
    }

    /**
     * Gets a list with all the users from the database and sets it to a listView adapter
     * and sets the adapter to the listView.
     */
    private void displayUsers() {
        final List<UserBean> users = UserStore.getInstance().getAllUsers();
        final ListAdapter adapter = new ListAdapter(getActivity(), R.layout.custom_list_item, users);
        listView.setAdapter(adapter);
        listView.setEmptyView(getActivity().findViewById(R.id.emptyElement));
    }

    static class ViewHolder {
        TextView name;
        TextView username;
        ImageView image;
    }

    /**
     * The adapter of the ListView which will contain the users from the database.
     */
    private class ListAdapter extends ArrayAdapter<UserBean> {
        private LayoutInflater inflater;

        public ListAdapter(Context context, int resource, List<UserBean> items) {
            super(context, resource, items);
            inflater = LayoutInflater.from(getContext());
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.custom_list_item, null);

                holder = new ViewHolder();
                holder.name = (TextView) convertView.findViewById(R.id.firstLine);
                holder.username = (TextView) convertView.findViewById(R.id.secondLine);
                holder.image = (ImageView) convertView.findViewById(R.id.icon);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            UserBean user = getItem(position);
            if (user != null) {
                if (holder.name != null) {
                    holder.name.setText(user.getName());
                }
                if (holder.username != null) {
                    holder.username.setText(user.getUsername());
                }
            }
            return convertView;
        }
    }
}
