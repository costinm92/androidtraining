package com.example.madalincostin.loginregister.persistence.bean.implementation;

import android.support.annotation.NonNull;

import com.example.madalincostin.loginregister.persistence.bean.Bean;

/**
 * Created by Madalin Costin
 * With Android Studio on 2015.
 */
public class UserFavoritesBean implements Bean, Comparable<UserFavoritesBean> {
    private String username;
    private String photoId;
    private Long timestamp;

    public UserFavoritesBean() {
    }

    public UserFavoritesBean(String username, String photoId, Long timestamp) {
        this.username = username;
        this.photoId = photoId;
        this.timestamp = timestamp;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhotoId() {
        return photoId;
    }

    public void setPhotoId(String photoId) {
        this.photoId = photoId;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public Object[] getIdentity() {
        return new Object[]{username, photoId};
    }

    @Override
    public boolean requiresEncryption() {
        return false;
    }

    @Override
    public int compareTo(@NonNull UserFavoritesBean another) {
        if (another.getPhotoId() != null && another.getUsername() != null) {
            return another.getPhotoId().compareTo(this.getPhotoId()) +
                    another.getUsername().compareTo(this.getUsername());
        }
        return 1;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((username == null) ? 0 : username.hashCode());
        result = prime * result + ((photoId == null) ? 0 : photoId.hashCode());
        result = prime * result + ((timestamp == null) ? 0 : timestamp.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (((Object) this).getClass() != obj.getClass()) {
            return false;
        }
        UserFavoritesBean other = (UserFavoritesBean) obj;
        if (username == null) {
            if (other.username != null) {
                return false;
            }
        } else if (!username.equals(other.username)) {
            return false;
        }
        if (photoId == null) {
            if (other.photoId != null) {
                return false;
            }
        } else if (!photoId.equals(other.photoId)) {
            return false;
        }
        if (timestamp == null) {
            if (other.timestamp != null) {
                return false;
            }
        } else if (!timestamp.equals(other.timestamp)) {
            return false;
        }
        return true;
    }
}