package com.example.madalincostin.loginregister.persistence.dao;

import com.example.madalincostin.loginregister.persistence.bean.Bean;

import java.util.List;

/**
 * Generic interface for the Data Access Objects methods and operations. Implemented by {@link AbstractSQLiteDAO}.
 *
 * @param <E> the sub-class of {@link Bean} that this DAO will be handling
 * @author Marian Lungu
 */
public interface DAO<E extends Bean> {

    /**
     * Insert an item
     *
     * @param item           the item that is inserted
     * @param updateIfExists use this parameter to have an "insert or update if exists" effect
     */
    void insert(E item, boolean updateIfExists);

    /**
     * insert a list of item
     *
     * @param updateIfExists if set to true, new items will be added and existing ones updated; if set to false, only new items are
     *                       added, existing one are not modified
     * @param items          the list of item that are added
     */
    void insert(List<E> items, boolean updateIfExists);

    /**
     * Update an item. If an item with the same identity can't be found in the table, this method will have no effect.
     *
     * @param item the item that is updated
     */
    void update(E item);

    /**
     * delete an item
     *
     * @param item the item that is deleted
     */
    void delete(E item);

    /**
     * delete an array of items
     *
     * @param items the items to be deleted
     */
    void deleteAll(E[] items);

    /**
     * delete all item
     */
    void deleteAll();

    /**
     * @return the list of all items
     */
    List<E> list();

    /**
     * @param pageSize maximum number of items returned/items per page
     * @param page
     * @return a page of all items
     */
    List<E> list(int pageSize, int page);

    /**
     * fetch an item with the specified identity
     *
     * @param identity SQL WHERE clause
     * @return the elements list that meet the filter
     */
    E get(Object... identity);

    /**
     * return the id of last inserted element
     *
     * @return the id of last inserted element
     */
    long getLastInsertedId();
}