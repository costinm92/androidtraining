package com.example.madalincostin.loginregister.persistence.dao.implementation;

import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.madalincostin.loginregister.persistence.Column;
import com.example.madalincostin.loginregister.persistence.bean.implementation.PhotoBean;
import com.example.madalincostin.loginregister.persistence.dao.AbstractSQLiteDAO;
import com.example.madalincostin.loginregister.persistence.dao.DAO;
import com.example.madalincostin.loginregister.persistence.dao.StatementBinder;

import java.util.Arrays;

import static com.example.madalincostin.loginregister.persistence.Column.CONSTRAINT_PRIMARY_KEY;
import static com.example.madalincostin.loginregister.persistence.Column.TYPE_TEXT;

/**
 * Created by Madalin Costin
 * With Android Studio on 2015.
 */
public class FavoritePhotosDAO extends AbstractSQLiteDAO<PhotoBean> implements DAO<PhotoBean> {
    protected static final String TABLE_NAME = "favorite_photos";

    protected static final Column[] COLUMNS = new Column[]{
            new Column("photo_id", TYPE_TEXT, CONSTRAINT_PRIMARY_KEY),// 0
            new Column("title", TYPE_TEXT), // 1
            new Column("url", TYPE_TEXT), // 2
    };

    public FavoritePhotosDAO(SQLiteOpenHelper helper) {
        super(helper);
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public Column[] getColumns() {
        return Arrays.copyOf(COLUMNS, COLUMNS.length);
    }

    @Override
    protected PhotoBean read(Cursor cursor) {
        PhotoBean favoritePhotosBean = new PhotoBean();
        favoritePhotosBean.setId(cursor.getString(0));
        favoritePhotosBean.setTitle(cursor.getString(1));
        favoritePhotosBean.setUrl(cursor.getString(2));
        return favoritePhotosBean;
    }

    @Override
    protected void fill(PhotoBean item, StatementBinder statement) {
        for (int i = 0; i < COLUMNS.length; i++) {
            bind(item, statement, i + 1, i);
        }
    }

    private void bind(PhotoBean item, StatementBinder statement, int bindingPos, int columnPos) {
        switch (columnPos) {
            case 0:
                statement.bindString(bindingPos, item.getId());
                break;
            case 1:
                if (item.getTitle() != null) {
                    statement.bindString(bindingPos, item.getTitle());
                }
                break;
            case 2:
                if (item.getUrl() != null) {
                    statement.bindString(bindingPos, item.getUrl());
                }
                break;
            default:
                break;
        }
    }
}
